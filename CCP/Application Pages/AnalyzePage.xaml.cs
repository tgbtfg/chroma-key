﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Controls;

namespace CCP
{
    /// <summary>
    /// Interaction logic for AnalyzePage.xaml
    /// </summary>
    public partial class AnalyzePage : Page
    {
        public AnalyzePage()
        {
            InitializeComponent();
        }

        private void ImageSelection_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            ScriptControl.InputSelectionBox.Text = ImageSelection.ImagePath;
        }



        private FileSystemWatcher fw;
        private void CaptureButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            PipelineManager manager = ControlPage.CurrentControlPage.pipelineManagerControl.GetManager();
            if (manager.isConnected)
            {
                if (manager.ComputePipeline is TTM_Module.TTMComputePipeline)
                {

                    string filepath = (manager.ComputePipeline as TTM_Module.TTMComputePipeline).CaptureCurrentFrame();
                    string dir = Path.GetDirectoryName(filepath);
                    fw = new FileSystemWatcher(dir)
                    {
                        EnableRaisingEvents = true,
                        Filter = "*.png"
                    };
                    fw.Created += (object s, FileSystemEventArgs ev) =>
                    {
                        ImageSelection.Dispatcher.Invoke(new Action(() => {
                            ImageSelection.SetImageFromFile(filepath);
                            }
                        ));
                        //if(ev.FullPath == filepath)
                            //ScriptControl.InputSelectionBox.Text = filepath;
                    };
                }
            }
        }
    }
}
