﻿using Microsoft.Win32;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using Newtonsoft.Json;
using TTMHostApp.TTMProcessHandling;
using CCP.TTM_Module;
using System.Collections.ObjectModel;
using CCP.Backend_Interface;
using System.Collections.Generic;
using System;

namespace CCP
{
    /// <summary>
    /// Interaction logic for ControlPage.xaml
    /// </summary>
    public partial class ControlPage : Page
    {

        public ControlPage()
        {
            InitializeComponent();
            CurrentControlPage = this;
        }

        public static ControlPage CurrentControlPage { get; internal set; }

    }
}
