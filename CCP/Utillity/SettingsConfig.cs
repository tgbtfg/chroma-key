﻿using CCP.Widgets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
namespace CCP.Utillity
{
    public abstract class Config: IFormattable
    {
        internal readonly Type type;
        internal Config(Type t) { type = t; }
        public abstract string ToString(string format, IFormatProvider formatProvider);
        public abstract void SetFromString(string parseString);
    }

    public class Config<T>: Config where T : struct, IConvertible
    {
        private static Dictionary<string, Config> registry = new Dictionary<string, Config>();

        private readonly string EntryName;
        public T Value {
            get { return Value; }
            set {
                ((Config<T>)registry[EntryName]).Value = value;
            }
        }

        public Config(string entryName, T value) : base(value.GetType())
        {
            if(!registry.ContainsKey(entryName))
            {
                this.EntryName = entryName;
                this.Value = value;
                registry.Add(entryName, (Config)this);
            }
            else
            {
                //throw new NotImplementedException();
                // bool.Parse("true");
                // error;
            }
        }

        public override string ToString(string format, IFormatProvider formatProvider)
        {
            return Value.ToString();
        }

        public override string ToString()
        {
            return Value.ToString();
        }

        public override void SetFromString(string parseString)
        {
            Value = (T)Convert.ChangeType(parseString, type);
        }

        public static void EditConfig()
        {
            SettingsConfigWindow configWindow = new SettingsConfigWindow();

            FormCreationDialogWindow inputDialog = new FormCreationDialogWindow
            {
                Title = "Edit global config"
            };
            foreach (var entry in registry)
            {
                string k = entry.Key;
                Config v = entry.Value;
                inputDialog.AddField(k, v.ToString());
            }
            if (inputDialog.ShowDialog() == true)
            {
                foreach (var entry in inputDialog.Answers)
                {
                    string key = entry.Key;
                    string val = entry.Value;

                    registry[key].SetFromString(val);
                }
            }
        }
    }
}
*/