﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CCP
{
    /// <summary>
    /// Interaction logic for AnalyzeWindow.xaml
    /// </summary>
    public partial class AnalyzeWindow : Window
    {
        private PipelineManager manager;

        public AnalyzeWindow(PipelineManager manager)
        {
            InitializeComponent();
            this.manager = manager;
            ScriptControl.outputPath = currentParameterFilePath;
            ScriptControl.SetScriptPath(currentScriptFilePath);
            // ScriptControl.ScriptSelectionBox.Text = currentScriptFilePath;
        }

        private void ImageSelection_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            ScriptControl.InputSelectionBox.Text = ImageSelection.ImagePath;
        }

        private FileSystemWatcher fw;
        private string currentParameterFilePath = @"D:\CCP\CCP\CCP\Scripting\parameterOutput.param";
        private string currentScriptFilePath = @"D:\CCP\CCP\CCP\Scripting\AnnotateBackground2.py";

        private void CaptureButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            PipelineManager manager = ControlPage.CurrentControlPage.pipelineManagerControl.GetManager();
            if (manager.isConnected)
            {
                if (manager.ComputePipeline is TTM_Module.TTMComputePipeline)
                {
                    string filepath = (manager.ComputePipeline as TTM_Module.TTMComputePipeline).CaptureCurrentFrame();
                    string dir = System.IO.Path.GetDirectoryName(filepath);
                    fw = new FileSystemWatcher(dir)
                    {
                        EnableRaisingEvents = true,
                        Filter = "*.png"
                    };
                    fw.Created += (object s, FileSystemEventArgs ev) =>
                    {
                        ImageSelection.Dispatcher.Invoke(new Action(() => {
                            ImageSelection.SetImageFromFile(filepath);
                            ScriptControl.RunScript();
                        }
                        ));
                        fw.EnableRaisingEvents = false; // One-shot
                    };
                }
            }
        }

        private void ApplyButton_Click(object sender, RoutedEventArgs e)
        {
            if (currentParameterFilePath != "")
            {
                if (File.Exists(currentParameterFilePath))
                {
                    manager.LoadParametersFromFile(currentParameterFilePath);
                    this.Close();
                }
                else
                {
                    PipelineManager.ShowMessage("No parameter file found.");
                }
            }
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
