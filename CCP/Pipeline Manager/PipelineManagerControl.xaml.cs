﻿using CCP.Backend_Interface;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CCP
{
    /// <summary>
    /// Interaction logic for PipelineManagerControl.xaml
    /// </summary>
    public partial class PipelineManagerControl : UserControl
    {
        PipelineManager manager;

        // string TypeText { get => TypeLabel.Content.ToString(); set => TypeLabel.Content = value; }
        // bool IsRunning { get => manager.isRunning; set => manager.isRunning = value; }
        // bool IsConnected { get => manager.isConnected; set => manager.isConnected = value; }
        // bool IsLocal { get => manager.; set => manager.isConnected = value; }

        //protected virtual void OnPropertyChanged(string property)
        //{
        //    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        //}

        //#region INotifyPropertyChanged Members
        //public event PropertyChangedEventHandler PropertyChanged;
        //#endregion


        public PipelineManagerControl()
        {
            InitializeComponent();
            manager = new PipelineManager(this);
            PipelineControlPanel.manager = manager;
        }

        private void NodeSelector_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            manager.ComputePipeline.SetView((sender as ComboBox).SelectedItem as string); // Set preview node to selection
        }

        private void ButtonOpen_Click(object sender, RoutedEventArgs e)
        {
            manager.OpenProject();
        }

        private void ButtonLoad_Click(object sender, RoutedEventArgs e)
        {
            manager.LoadParameters();
        }

        private void ButtonSave_Click(object sender, RoutedEventArgs e)
        {
            manager.SaveParameters();
        }

        private void ButtonConnect_Click(object sender, RoutedEventArgs e)
        {
            manager.Connect();
            isConnectedLabel.Content = manager.isConnected.ToString();
            isRunningLabel.Content = manager.isRunning.ToString();

            if (manager.isConnected)
            {
                if (manager.ComputePipeline is TTM_Module.TTMComputePipeline) TypeLabel.Content = "TTM";
                if (manager.ComputePipeline is TTM_Module.TTM2ComputePipeline) TypeLabel.Content = "TTM2";
            }
        }

        private void ButtonRunPipeline_Click(object sender, RoutedEventArgs e)
        {
            manager.RunPipeline();
        }

        public void PopulateNodeViewFromPipeline(IPipelineGraph pipelineModel)
        {
            // Populate node view list
            List<IPipelineNode> nodes = pipelineModel?.GetNodes();
            if (nodes != null)
            {
                List<string> nodeNames = new List<string>();
                foreach (IPipelineNode node in nodes) { nodeNames.Add(node.name); }

                NodeSelector.ItemsSource = new ObservableCollection<string>(nodeNames);
                PipelineControlPanel.BuildFromPipeline(pipelineModel);
            }
        }

        private void ButtonCaptureFrame_Click(object sender, RoutedEventArgs e)
        {
            /*
            if (manager.isConnected)
            {
                if (manager.ComputePipeline is TTM_Module.TTMComputePipeline)
                {
                    string filepath = (manager.ComputePipeline as TTM_Module.TTMComputePipeline).CaptureCurrentFrame();
                }
            }
            */
            AnalyzeWindow analyzeWindow = new AnalyzeWindow(manager);
            analyzeWindow.Show();
        }

        internal void AddImageSlot(string filename)
        {
            ImageSlot slot = new ImageSlot();
            slot.SetImageFromFile(filename);
            //DockPanel.SetDock(slot, Dock.Right);

            Gallery.Children.Add(slot);
        }

        public PipelineManager GetManager()
        {
            return manager;
        }
    }
}
