﻿using Microsoft.Win32;
using System.IO;
using System.Windows;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;

namespace CCP
{
    public class PipelineManager : IPipelineManager
    {
        //public IComputePipeline ComputePipeline { get; internal set; }
        private Window HostWindow { get; set; }
        public IComputePipeline ComputePipeline { get; set; }
        public ParameterControlRegistry CurrentParameterControlRegistry { get; set; } = new ParameterControlRegistry();

        // TODO: ParameterControlRegistry should perhaps not be created here.
        // TODO: The ParameterControlRegistry should handle saving and loading of parameters and warn before being unloaded
        // public ParameterControlRegistry CurrentParameterControlRegistry = new ParameterControlRegistry();

        public bool isRunning = false;
        public bool isConnected = false;
        private PipelineManagerControl pipelineManagerControl;

        public PipelineManager(PipelineManagerControl pipelineManagerControl)
        {
            this.pipelineManagerControl = pipelineManagerControl;
        }

        public void Connect()
        {
            // TODO: this is TTM specific. refactor?
            //if (client == null) // Need new connection if reconnecting to different server

            FormDialogWindow inputDialog = new FormDialogWindow { Title = "Connect to server" };
            inputDialog.AddField("IP:", "127.0.0.1");
            inputDialog.AddField("Port:", "27015");
            inputDialog.AddComboBox("Pipeline:", new List<string>{ "TTM", "TTM2"});
            if (inputDialog.ShowDialog() == true)
            {
                string pipelineAnswer = inputDialog.Answers["Pipeline:"];
                if (pipelineAnswer == "TTM")
                {
                    if(!isRunning) ComputePipeline = new TTM_Module.TTMComputePipeline(pipelineManagerControl);
                    isConnected |= ComputePipeline.Connect(inputDialog.Answers["IP:"], inputDialog.Answers["Port:"]);
                }
                else if (pipelineAnswer == "TTM2")
                {
                    ComputePipeline = new TTM_Module.TTM2ComputePipeline();
                    //client2 = new TTM_Module.TTM2.TTM2SocketClientTCP();
                    isConnected |= ComputePipeline.Connect(inputDialog.Answers["IP:"], inputDialog.Answers["Port:"]);
                }
                else
                {
                    ShowMessage("Pipeline type not available");
                }
            }
        }

        public void RunPipeline()
        {
            RunTTMPipeline();
        }

        private void RunTTMPipeline()
        {
            HostWindow = new Window();

            CCP.TTM_Module.TTMComputePipeline computepipeline = new CCP.TTM_Module.TTMComputePipeline(pipelineManagerControl);
            //computepipeline.managerControl = pipelineManagerControl;
            TTMHostApp.TTMProcessHandling.ProcessContainerControl TTMControl = new TTMHostApp.TTMProcessHandling.ProcessContainerControl(computepipeline);
            ComputePipeline = computepipeline;

            string filename = "";
            OpenFileDialog openFileDialog = new OpenFileDialog
            {
                // Set filter for file extension and default file extension 
                DefaultExt = ".exe",
                Filter = "exe Files (*.exe)|*.exe",
                Title = "Choose TTM executable"
            };

            if (openFileDialog.ShowDialog() == true)
            {
                filename = openFileDialog.FileName;
                filename = filename.Replace("\\", "/");
                TTMControl.ExeName = filename;
            }
            else
            {
                // TODO: Remove this, or automate locating TTM
                TTMControl.ExeName = "D:\\Perforce\\tgb_TJCGWS007_TTM-Dev\\binaries\\TTMPipelined.exe";
            }

            HostWindow.Content = TTMControl;

            // Unloaded += (object s, RoutedEventArgs e) => HostWindow.Close(); // TODO: Change from unloaded to some kind of onExit
            HostWindow.Show();
            isRunning = true;
        }

        public void SaveParameters()
        {
            if (!isRunning) { ShowMessage("Pipeline not running"); return; }
            ParameterMap settings = CurrentParameterControlRegistry.CreateParameterMapFromControls();

            SaveFileDialog saveFileDialog = new SaveFileDialog
            {
                // Set filter for file extension and default file extension 
                // DefaultExt = ".param",
                // Filter = "CCP Parameter Files (*.param)|*.param"
            };

            if (saveFileDialog.ShowDialog() == true)
            {
                string path = saveFileDialog.FileName;
                string JSONOutput = JsonConvert.SerializeObject(settings);
                File.WriteAllText(saveFileDialog.FileName, JSONOutput);
            }
        }

        public void LoadParameters()
        {
            if (!isRunning) { ShowMessage("Pipeline not running"); return; }
            OpenFileDialog loadFileDialog = new OpenFileDialog
            {
                // Set filter for file extension and default file extension 
                // DefaultExt = ".param",
                // Filter = "CCP Parameter Files (*.param)|*.param"
            };

            if (loadFileDialog.ShowDialog() == true)
            {
                string path = loadFileDialog.FileName;
                LoadParametersFromFile(path);
                //string JSONInput = File.ReadAllText(loadFileDialog.FileName);
                //ParameterMap parameters = JsonConvert.DeserializeObject<ParameterMap>(JSONInput);
                //ApplyParameterMap(parameters);
            }
        }

        public void LoadParametersFromFile(string path)
        {
            try
            {
                string JSONInput = File.ReadAllText(path);
                //Dictionary<string, Dictionary<string, object>> parameters2 = JsonConvert.DeserializeObject<Dictionary<string, Dictionary<string, object>>>(JSONInput);
                ParameterMap parameters = JsonConvert.DeserializeObject<ParameterMap>(JSONInput);
                ApplyParameterMap(parameters);
            } catch(Exception e){ Console.WriteLine(e.ToString()); }
        }

        private void ApplyParameterMap(ParameterMap settings)
        {
            // Loop over each node, and parameters within each node, and update the parameters in the pipeline
            // based on the values in the paramter map
            foreach (var node in settings)
                foreach (var parameter in node.Value)
                    if(parameter.Value is JToken)
                        SetParameter(node.Key, parameter.Key, parameter.Value);
                    else
                        SetParameter(node.Key, parameter.Key, JToken.FromObject(parameter.Value));
        }

        public void SetParameter(string nodeName, string parameterName, object parameterValues)
        {
            ComputePipeline.SetParameter(nodeName, parameterName, parameterValues); // Sends parameter update to compute pipeline.
            CurrentParameterControlRegistry.SetControlParameterValue(nodeName, parameterName, parameterValues); // Sets local copy of parameters and updates UI controls
        }

        public void OpenProject()
        {
            if (!isRunning || !isConnected) { ShowMessage("Not connected to pipeline"); return; }
            OpenFileDialog openFileDialog = new OpenFileDialog
            {
                // Set filter for file extension and default file extension 
                DefaultExt = ".ttmp",
                Filter = "TTMP Files (*.ttmp)|*.ttmp"
            };

            if (openFileDialog.ShowDialog() == true)
            {
                ComputePipeline.OpenProject(openFileDialog.FileName);
            }
        }

        public static void ShowMessage(string message)
        {
            string caption = "Message";
            MessageBoxButton button = MessageBoxButton.OK;
            MessageBoxImage icon = MessageBoxImage.Warning;
            MessageBoxResult result = MessageBox.Show(message, caption, button, icon);
        }
    }
}
