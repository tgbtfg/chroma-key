﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace CCP
{
    public class ParameterControlMap : Dictionary<string, Dictionary<string, IParameterControl>>
    {
        // TODO: Construct based on pipeline nodes and and parameters
        // TODO: Update entries based on Pipeline changes
        // TODO: Communicate with PipelineControl to synchronize values
        // TODO: Create save/serialize, and load/deserialize fuctionality using JSON

        // public Dictionary<string, Dictionary<string, string>> NodeParameterMap;
        public class ControlEntries : Dictionary<string, IParameterControl> { };
    }

    public class ParameterControlRegistry
    {
        private ParameterControlMap ControlRegistry = new ParameterControlMap();

        public ParameterControlRegistry() {}

        public void RegisterParameterControl(IParameterControl control, string nodeName, string parameterName)
        {
            // TODO fixme: THIS EXPLODES WHENEVER ADDING DUPLICATE ENTRY ( ArgumentException )
            try
            {
                if(ControlRegistry.ContainsKey(nodeName))
                {
                    ControlRegistry[nodeName].Add(parameterName, control);
                }
                else
                    ControlRegistry.Add(nodeName,
                        new ParameterControlMap.ControlEntries()
                        {
                            [parameterName] = control
                        }
                    );
            }
            catch (ArgumentException e) // TODO: This is probably not sufficient
            {
                Console.WriteLine(e.ToString()); // TODO: handle this better
            }
        }

        public ParameterMap CreateParameterMapFromControls()
        {
            ParameterMap settings = new ParameterMap();
            foreach (var nodeEntry in ControlRegistry)
            {
                foreach (var parameterEntry in nodeEntry.Value)
                {
                    var control = parameterEntry.Value;
                    if (settings.ContainsKey(nodeEntry.Key)) // If node entry already exists, just add parameter name and string
                    {
                        settings[nodeEntry.Key].Add(parameterEntry.Key, control.GetParameterValue());
                    }
                    else // Otherwise add new node entry with a single element corresponding to the parameter
                    settings[nodeEntry.Key] = new ParameterMap.NodeEntries
                    {
                        [parameterEntry.Key] = control.GetParameterValue()
                    };
                }
            }
            return settings;
        }

        public void SetControlParameterValue(string nodeName, string parameterName, object parameterValues)
        {
            try
            {
                if(parameterValues is JToken)
                {
                    JToken token = (parameterValues as JToken);
                    // TODO fixme: THIS EXPLODES WHENEVER ENTRY IS NOT FOUND, OR ADDING DUPLICATE ENTRY
                    IParameterControl paramControl = ControlRegistry[nodeName][parameterName];
                    if (paramControl is IParameterControl<double>)
                    {
                        //(paramControl as IParameterControl<double>).ParameterValue =
                        //    double.Parse(parameterValues, System.Globalization.CultureInfo.InvariantCulture);
                        (paramControl as IParameterControl<double>).ParameterValue = token.ToObject<Double>();
                    }
                    else
                    {
                        //List<string> paramList = parameterValues.Split(',').ToList();
                        //List<double> valueList = parameterValues.Split(',').ToList().ConvertAll<double>(a => double.Parse(a, System.Globalization.CultureInfo.InvariantCulture));
                        //List<double> valueList = JsonConvert.DeserializeObject<List<double>>(parameterValues);
                        List<double> valueList = token.ToObject<List<double>> ();
                        if (paramControl is IParameterControl<Color> && valueList.Count == 4)
                        {
                            byte toByte(double x) => (byte)(x * 255);
                        
                            byte r = toByte(valueList[0]);
                            byte g = toByte(valueList[1]);
                            byte b = toByte(valueList[2]);
                            byte a = toByte(valueList[3]);
                            (paramControl as IParameterControl<Color>).ParameterValue = Color.FromArgb(a, r, g, b);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }
    }
}
