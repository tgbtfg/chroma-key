﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CCP
{
    public interface IPipelineManager
    {
        IComputePipeline ComputePipeline { get; set; }
        ParameterControlRegistry CurrentParameterControlRegistry { get; set; }
        void SetParameter(string nodeName, string parameterName, object parameterValues);
    }
}
