﻿using CCP.Backend_Interface;
using System;
using System.Collections.Generic;
using System.Windows.Controls;

namespace CCP
{
    /// <summary>
    /// Interaction logic for PipelineControl.xaml
    /// </summary>
    public partial class PipelineControl : UserControl
    {
        public IPipelineManager manager;

        public PipelineControl()
        {
            InitializeComponent();
        }


        public void AddPipelineNode(IPipelineNode node)
        {
            PipelineControlStack.Children.Add(new NodeControl(manager, node));
        }

        public void BuildFromPipeline(IPipelineGraph pipeline)
        {
            // TODO: warn about unsaved changes, if detected, before scrapping current contents.
            // TODO: Check where this is called from and whether warning should occur someplace else

            // Set title to project name
            //TitleLabel.Content = pipeline.ToString(); // Temp
            SetTitle(pipeline.ToString());
            PipelineControlStack.Children.RemoveRange(0, PipelineControlStack.Children.Count);

            // Add NodeControls for each node
            foreach (IPipelineNode node in pipeline.GetNodes())
            {
                AddPipelineNode(node);
            }
        }

        internal void SetTitle(string NewTitle)
        {
            TitleLabel.Content = NewTitle;
        }

        private void AddNodeButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            FormDialogWindow nodeCreationDialog = new FormDialogWindow();
            //nodeCreationDialog.AddField("Pipeline:", "TTM");
            //nodeCreationDialog.AddComboBox("Pipeline:", new List<string> { "TTM", "TTM2" });
            nodeCreationDialog.AddField("nodename:", "new node");
            nodeCreationDialog.AddComboBox("nodetype:", new List<string> { "Input", "ComputeShader", "Output" });

            if (nodeCreationDialog.ShowDialog() == true)
            {
                NodeType t = NodeType.ComputeShader;
                try
                {
                    if (nodeCreationDialog.Answers["nodetype:"] == null) { PipelineManager.ShowMessage("Node type type not available"); }
                    else { Enum.TryParse(nodeCreationDialog.Answers["nodetype:"], out t); }
                } catch (Exception ex) { PipelineManager.ShowMessage("Exception thrown:\n" + ex.ToString()); }
                
                if (manager.ComputePipeline is TTM_Module.TTMComputePipeline)
                {
                    AddPipelineNode(new CPP.TTM_Module.TTMPipeline.Node()
                    {
                        name = nodeCreationDialog.Answers["nodename:"],
                        type = t
                    });
                }
                else
                if (manager.ComputePipeline is TTM_Module.TTM2ComputePipeline)
                {
                    AddPipelineNode(new CPP.TTM_Module.TTMPipeline.Node()
                    {
                        name = nodeCreationDialog.Answers["nodename:"],
                        type = t
                    });
                }
                else
                {
                    if(manager.ComputePipeline != null) PipelineManager.ShowMessage("Error: Unknown pipeline type.");
                    else PipelineManager.ShowMessage("No pipeline connected.");

                }
            }
        }
    }
}
