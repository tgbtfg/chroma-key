﻿using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Shapes;

namespace CCP
{
    /// <summary>
    /// Interaction logic for InputSurface.xaml
    /// </summary>
    public partial class InputSurface : UserControl, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        
        IPipelineManager manager;

        public InputSurface(IPipelineManager manager)
        {
            this.manager = manager;
            InitializeComponent();
            PropertyChanged += InputSurface_PropertyChanged;
        }
        /*
        public InputSurface()
        {
            InitializeComponent();
            PropertyChanged += InputSurface_PropertyChanged;
        }
        */

        string parameterName = "Param";
        public string ParameterName
        {
            get { return parameterName; }
            set { parameterName = value; }
        }

        public string ParameterTitle
        {
            get { return ParameterTitleLabel.Content.ToString(); }
            set { ParameterTitleLabel.Content = value; }
        }

        public string NodeName { get; set; } = null;

        private double xmin = 0;
        private double xmax = 1;
        private double ymin = 0;
        private double ymax = 1;

        public bool CheckBounds(Point p)
        {
            return
                xmin < p.X && p.X < xmax &&
                ymin < p.Y && p.Y < ymax;
        }

        private Point ParamCoordToCanvasCoord(Point p)
        {
            Point p2 = p;
            p2.X = ControlCanvas.Width * (p2.X - xmin) / (xmax - xmin);
            p2.Y = ControlCanvas.Height * (p2.Y - ymin) / (ymax - ymin);
            return p2;
        }

        private Point CanvasCoordToParamCoord(Point p)
        {
            Point p2 = p;
            p2.X = (xmax - xmin) * p2.X/ControlCanvas.Width + xmin;
            p2.Y = (ymax - ymin) * p2.Y / ControlCanvas.Height + ymin;
            return p2;
        }

        private Point parameterValue;
        public Point ParameterValue
        {
            get { return parameterValue; }
            set
            {
                if (parameterValue != value && CheckBounds(value))
                {
                    parameterValue = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("ParameterValue"));
                    SetControlPointPosition(parameterValue);
                }
            }
        }

        public string GetSetParamValueString()
        {
            string command = "node:set_param_value('" + parameterName + "', ";
            command += parameterValue.X.ToString("0.0000", System.Globalization.CultureInfo.InvariantCulture) + ", ";
            command += parameterValue.Y.ToString("0.0000", System.Globalization.CultureInfo.InvariantCulture);
            command += ")";
            return command;
        }

        private void InputSurface_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "ParameterValue" && manager.ComputePipeline.IsInitialized())
            {
                string nodeName = NodeName ?? manager.ComputePipeline.GetView();
                string parameterValueString = "";
                parameterValueString += parameterValue.X.ToString("0.0000", System.Globalization.CultureInfo.InvariantCulture) + ", ";
                parameterValueString += parameterValue.Y.ToString("0.0000", System.Globalization.CultureInfo.InvariantCulture);
                //PipelineManager.ComputePipeline.SetParameter(nodeName, parameterName, parameterValueString);
                manager.SetParameter(nodeName, parameterName, parameterValueString);
            }
        }


        public void SetControlPointPosition(Point p)
        {
            Point p2 = ParamCoordToCanvasCoord(p);
            ControlPoint.SetValue(Canvas.LeftProperty, p2.X);
            ControlPoint.SetValue(Canvas.TopProperty, p2.Y);
        }

        private void MoveControlPoint(MouseEventArgs e)
        {
            Point canvPosToWindow = ControlCanvas.TransformToAncestor(this).Transform(new Point(0, 0));

            Ellipse r = ControlPoint;
            var upperlimit = canvPosToWindow.Y + (r.Height / 2);
            var lowerlimit = canvPosToWindow.Y + ControlCanvas.ActualHeight - (r.Height / 2);

            var leftlimit = canvPosToWindow.X + (r.Width / 2);
            var rightlimit = canvPosToWindow.X + ControlCanvas.ActualWidth - (r.Width / 2);
            
            Point p = e.GetPosition(this);

            if ((p.X > leftlimit && p.X < rightlimit)
                && (p.Y > upperlimit && p.Y < lowerlimit))
            {
                Point p2 = e.GetPosition(ControlCanvas);
                p2.X -= (r.Width / 2);
                p2.Y -= (r.Height / 2);
                p2.X /= ControlCanvas.Width;
                p2.Y /= ControlCanvas.Height;

                ParameterValue = p2;
            }
        }

        private bool isDragging;

        private void ControlCanvas_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            ControlPoint.CaptureMouse();
            isDragging = true;
            MoveControlPoint(e as MouseEventArgs);
        }

        private void ControlCanvas_MouseMove(object sender, System.Windows.Input.MouseEventArgs e)
        {
            if (isDragging)
            {
                MoveControlPoint(e);
            }
        }

        private void ControlCanvas_MouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            ControlPoint.ReleaseMouseCapture();
            isDragging = false;
        }

        private void ParameterTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (sender is TextBox box)
                try { ParameterValue = Point.Parse(box.Text); }
                catch { box.Text = ParameterValue.ToString(System.Globalization.CultureInfo.InvariantCulture); }
        }
    }
}
