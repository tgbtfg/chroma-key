﻿// TODO: Refactor, decide whether to remove TTM specific stuff, or move control to TTM module
// Alternatively, create interface to abstract away specifics and move implementation to TTM module

using CCP.Backend_Interface;
using System.Collections.Generic;
using System.Windows.Controls;

namespace CCP
{
    /// <summary>
    /// Interaction logic for NodeControl.xaml
    /// 
    /// Is automatically populated with parameter controls
    /// from a designated node
    /// 
    /// </summary>
    public partial class NodeControl : UserControl
    {
        /*
        public NodeControl()
        {
            InitializeComponent();
        }
        public NodeControl(IPipelineManager manager)
        {
            this.manager = manager;
            InitializeComponent();
        }
        */

        public IPipelineNode PipelineNode { get; set; }

        private ParameterSliderStack SliderStack { get; set; }

        internal ImageSlot inputImageSlot = null; // not null only if input node

        private IPipelineManager manager;

        private void NodeSetup(IPipelineNode node)
        {
            List<IParameter> ParameterList = node.GetParameters();             
            if (ParameterList != null)
            {
                foreach (IParameter param in ParameterList)
                {
                    switch (param.type)
                    {
                        case ("float"):
                            {
                                // TODO clean up this (?)
                                double minValue = 0;
                                double maxValue = 1;
                                double stepSize = 0.001;
                                double defaultValue = 0;
                                string parameterLabelTitle = param.type + ": " + param.name;
                                SliderStack.AddSlider(param.name, minValue, maxValue, stepSize, defaultValue, parameterLabelTitle);
                            }
                            break;
                        case ("float2"):
                            {
                                InputSurface surf = new InputSurface(manager)
                                {
                                    ParameterName = param.name,
                                    ParameterTitle = "float2: " + param.name,
                                    NodeName = node.name
                                };

                                NodeControlStack.Children.Add(surf);

                            }
                            break;
                        case ("float3"):
                            {
                                //ParameterColorPickerControl picker = new ParameterColorPickerControl(manager);
                                //NodeControlStack.Children.Add(picker);
                            }
                            break;
                        case ("float4"):
                            {
                                ParameterColorPickerControl picker = new ParameterColorPickerControl(manager, node, param);
                                NodeControlStack.Children.Add(picker);
                                manager.CurrentParameterControlRegistry.RegisterParameterControl(picker, node.name, param.name);
                            }
                            break;

                            //TODO: default case
                            //TODO: distinguish between float2,3,4 interpretation: Color, position, direction, normal, etc
                    }
                }
            }

            
            switch (node.type)
            {
                case (NodeType.ComputeShader):
                    { }
                    break;

                case (NodeType.Input):
                    {
                       if (node.isImageNode())
                       {
                            inputImageSlot = new ImageSlot();
                            NodeControlStack.Children.Add(inputImageSlot);
                            inputImageSlot.PropertyChanged += (_, __) =>
                            {
                                if (manager.ComputePipeline.IsInitialized())
                                    manager.ComputePipeline.SetInputImage(inputImageSlot); // TODO: rework this
                            };
                        }
                    }
                    break;
                case (NodeType.Output):
                    { }
                    break;
                default:
                    break;
            }
        }

        public NodeControl(IPipelineManager manager, IPipelineNode node)
        {
            InitializeComponent();
            this.manager = manager;
            PipelineNode = node;

            SliderStack = new ParameterSliderStack(manager);
            switch(node.type)
            {
                case (NodeType.ComputeShader):
                    {
                        SliderStack.TitleLabel.Content = node.name + " (Compute Shader Node)";
                    }
                    break;
                case (NodeType.Input):
                    {
                        SliderStack.TitleLabel.Content = node.name + " (Input Node)";
                    }
                    break;
                case (NodeType.Output):
                    {
                        SliderStack.TitleLabel.Content = node.name + " (Output Node)";
                    }
                    break;
                default:
                    SliderStack.TitleLabel.Content = node.name;
                    break;
            }
            
            SliderStack.NodeName = node.name;
            NodeControlStack.Children.Add(SliderStack);

            NodeSetup(node);
        }
    }
}
