﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Windows;
using System.Windows.Controls;

namespace CCP
{
    /// <summary>
    /// Interaction logic for ParameterSliderControl.xaml
    /// </summary>
    public partial class ParameterSliderControl : UserControl, IParameterControl<double>
    {
        private double parameterValue = 0;
        private double defaultValue   = 0;
        private double maximumValue   = 0;
        private double minimumValue   = 0;
        private double stepSizeValue  = 0;
        private string parameterName = "Param";

        public event RoutedPropertyChangedEventHandler<double> ValueChanged;
        protected virtual void OnValueChanged(RoutedPropertyChangedEventArgs<double> e)
        {
            ValueChanged?.Invoke(this, e);
        }

        public double DefaultValue
        {
            get { return defaultValue; }
            set { defaultValue = value; ParameterSlider.Value = defaultValue; }
        }

        public double MaximumValue
        {
            get { return maximumValue; }
            set { maximumValue = value; ParameterSlider.Maximum = maximumValue; }
        }

        public double MinimumValue
        {
            get { return minimumValue; }
            set { minimumValue = value; ParameterSlider.Minimum = MinimumValue; }
        }

        public double StepSize
        {
            get { return stepSizeValue; }
            set {
                stepSizeValue = value;
                ParameterSlider.SmallChange = stepSizeValue;
                ParameterSlider.LargeChange = stepSizeValue;
            }
        }
        public double ParameterValue
        {
            get { return parameterValue; }
            set
            {
                if (value != parameterValue)
                {
                    parameterValue = value;
                    this.ParameterTextBox.Text = parameterValue.ToString("0.0000", System.Globalization.CultureInfo.InvariantCulture);
                    this.ParameterSlider.Value = value;
                }
            }
        }
        public string ParameterName
        {
            get { return parameterName; }
            set { parameterName = value; }
        }

        public string ParameterTitle
        {
            get { return ParameterLabel.Content.ToString(); }
            set { ParameterLabel.Content = value; }
        }

        public ParameterSliderControl()
        {
            InitializeComponent();
        }

        private void Slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            ParameterValue = e.NewValue;
            OnValueChanged(e);
        }

        private void ParameterTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            try {
                double result = double.Parse((sender as TextBox).Text, System.Globalization.CultureInfo.InvariantCulture);
                ParameterValue = result;
            }
            catch (Exception) { }
            //if (double.TryParse((sender as TextBox).Text, out double result)) {}
        }
        
        object IParameterControl.GetParameterValue()
        {
            //string str = ParameterValue.ToString(System.Globalization.CultureInfo.InvariantCulture);
            //string jsonstring = JsonConvert.SerializeObject(ParameterValue);
            //return jsonstring;
            return JToken.FromObject(parameterValue);

        }
        
    }
}
