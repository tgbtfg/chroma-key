﻿using CCP.Backend_Interface;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CCP
{
    /// <summary>
    /// Interaction logic for ParameterColorPickerControl.xaml
    /// </summary>
 
    /*
    class Float4 : IFormattable
    {
        public float r, g, b, a;

        string IFormattable.ToString(string format, IFormatProvider formatProvider)
        {
            return "" + r + ", " + g + ", " + b + ", " + a;
        }

        override public string ToString()
        {
            return "" + r + ", " + g + ", " + b + ", " + a;
        }
    }
    */
    public partial class ParameterColorPickerControl : UserControl, IParameterControl<Color>
    {
        Xceed.Wpf.Toolkit.ColorPicker picker = new Xceed.Wpf.Toolkit.ColorPicker();

        private IPipelineManager manager;

        private IPipelineNode node;

        void OnChangeColor(object sender, EventArgs e)
        {
            if(picker.SelectedColor != null)
                color = (Color)picker.SelectedColor;

            if (manager.ComputePipeline?.IsInitialized() != null)
            {
                string nodeName = node?.name ?? manager.ComputePipeline.GetView();
                manager.SetParameter(nodeName, parameterName, (this as IParameterControl).GetParameterValue());
            }
        }

        public ParameterColorPickerControl(IPipelineManager manager, IPipelineNode node, IParameter param)
        {
            InitializeComponent();
            picker.SelectedColorChanged += OnChangeColor;
            ContainerStack.Children.Add(picker);
            this.manager = manager;
            this.node = node;
            (this as IParameterControl<System.Windows.Media.Color>).ParameterName = param.name;
            //manager.CurrentParameterControlRegistry.RegisterParameterControl(this, node.name, parameterName);
        }

        Color color = new Color();
        string parameterName = "";

        Color IParameterControl<Color>.ParameterValue { get => color;
            set {
                if (color != value)
                {
                    color = value;
                    picker.SelectedColor = color;
                }
            }
        }

        string IParameterControl<Color>.ParameterName { get => parameterName;
            set
            {
                parameterName = value;
                ParameterLabel.Content = value;
            }
        }

        object IParameterControl.GetParameterValue()
        {
            //string r = (color.R / 255.0).ToString("0.0000", System.Globalization.CultureInfo.InvariantCulture) + ", ";
            //string g = (color.G / 255.0).ToString("0.0000", System.Globalization.CultureInfo.InvariantCulture) + ", ";
            //string b = (color.B / 255.0).ToString("0.0000", System.Globalization.CultureInfo.InvariantCulture) + ", ";
            //string a = (color.A / 255.0).ToString("0.0000", System.Globalization.CultureInfo.InvariantCulture);
            //string str = r + g + b + a;

            double r = (color.R / 255.0);
            double g = (color.G / 255.0);
            double b = (color.B / 255.0);
            double a = (color.A / 255.0);

            List<double> paramList = new List<double> { r, g, b, a };

            //string str = JsonConvert.SerializeObject(paramList);

            return JToken.FromObject(paramList);

            //return str;
            //return color.ToString();
        }
        
    }
}
