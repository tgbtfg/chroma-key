﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

namespace CCP
{
    /// <summary>
    /// Interaction logic for ParameterSliderStack.xaml
    /// </summary>
    public partial class ParameterSliderStack : UserControl
    {
        private IPipelineManager manager;

        private string title = "Slider Stack Panel";

        public ParameterSliderStack(IPipelineManager manager)
        {
            this.manager = manager;
            InitializeComponent();
            Title = "Slider Stack Panel";
        }

        public string Title
        {
            get { return title;  }
            set { title = value;  this.TitleLabel.Content = title; }
        }

        public string NodeName { get; set; } = null;

        public void UpdateSliderLabelWidths()
        {
            double MaxWidth = 0;
            foreach (ParameterSliderControl psc in this.SliderPanel.Children)
            {
                psc.ParameterLabel.Measure(new Size(Double.PositiveInfinity, Double.PositiveInfinity));
                MaxWidth = Math.Max(MaxWidth, psc.ParameterLabel.DesiredSize.Width);
            }

            foreach (ParameterSliderControl psc in this.SliderPanel.Children)
            {
                psc.ParameterLabel.Width = MaxWidth;
            }
            this.UpdateLayout();
        }

        public void AddSlider(string Name, double minValue = 0, double maxValue = 1, double  stepsize = 0.001, double defaultValue = 0, string Title = null)
        {
            ParameterSliderControl NewSlider = new ParameterSliderControl
            {
                MinimumValue = minValue,
                MaximumValue = maxValue,
                StepSize = stepsize,
                DefaultValue = defaultValue,
                ParameterName = Name
            };

            NewSlider.ParameterLabel.Content = Title ?? Name;
            NewSlider.ValueChanged += ParameterSlider_ValueChanged;
            this.SliderPanel.Children.Add(NewSlider);
            this.UpdateLayout();
            UpdateSliderLabelWidths();

            // TODO: perhaps reconsider directly accessing the pipeline manager from here
            //if(PipelineManager.ComputePipeline?.IsInitialized() != null)
            {
                //PipelineManager.ComputePipeline.Connect(NewSlider, NodeName, Name);
                manager.CurrentParameterControlRegistry.RegisterParameterControl(NewSlider, NodeName, Name);
            }
            //else PipelineManager.ShowMessage("Warning: Slider will not be connected. Pipeline is not initialized, or is not set.");
        }

        private void ParameterSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (manager.ComputePipeline?.IsInitialized() != null)
            {
                string nodeName = NodeName ?? manager.ComputePipeline.GetView();
                ParameterSliderControl Control = sender as ParameterSliderControl;
                //string parameterValueString = Control.ParameterValue.ToString("0.0000", System.Globalization.CultureInfo.InvariantCulture);
                object parameterValue = (Control as IParameterControl).GetParameterValue();
                manager.SetParameter(nodeName, Control.ParameterName, parameterValue);
            }
        }

        public ParameterSliderControl GetSlider(string Name)
        {
            foreach(ParameterSliderControl psc in this.SliderPanel.Children)
            {
                if (String.Equals(Name, psc.ParameterName, StringComparison.OrdinalIgnoreCase)) return psc;
            }
            return null;
        }

        public List<string> GetAllSliderNames()
        {
            List<string> sliderList = new List<string>();
            foreach (ParameterSliderControl psc in this.SliderPanel.Children)
            {
                sliderList.Add(psc.ParameterName);
            }
            return sliderList;
        }

        public void RemoveSlider(string Name)
        {
            this.SliderPanel.Children.Remove(GetSlider(Name));
            this.UpdateLayout();
            UpdateSliderLabelWidths();
        }

        private void AddSliderButton_Click(object sender, RoutedEventArgs e)
        {
            FormDialogWindow inputDialog = new FormDialogWindow();
            inputDialog.Title = "Create new parameter";
            inputDialog.AddField("Parameter name:", "param0");
            inputDialog.AddField("Minimum value:", "0.");
            inputDialog.AddField("Maximum value:", "10.");
            inputDialog.AddField("Step size:", "0.1");
            inputDialog.AddField("Default value:", "0.");
            if (inputDialog.ShowDialog() == true)
            {
                try
                {
                    AddSlider(
                        inputDialog.Answers["Parameter name:"],
                        double.Parse(inputDialog.Answers["Minimum value:"], System.Globalization.CultureInfo.InvariantCulture),
                        double.Parse(inputDialog.Answers["Maximum value:"], System.Globalization.CultureInfo.InvariantCulture),
                        double.Parse(inputDialog.Answers["Step size:"], System.Globalization.CultureInfo.InvariantCulture),
                        double.Parse(inputDialog.Answers["Default value:"], System.Globalization.CultureInfo.InvariantCulture)
                    );
                }
                catch (System.FormatException)
                {
                    AddSlider("param");
                }
            }
        }

        private void RemoveSliderButton_Click(object sender, RoutedEventArgs e)
        {
            FormDialogWindow inputDialog = new FormDialogWindow();
            List<string> labels = GetAllSliderNames();
            inputDialog.AddComboBox("Parameter name:", labels);
            if (inputDialog.ShowDialog() == true) {
                RemoveSlider( inputDialog.Answers["Parameter name:"] );
            }
        }

        private void onParameterChanged()
        {

        }
    }
}
