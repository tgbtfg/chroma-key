﻿# Assumes Python 3.6 or later
# requires numpy, and opencv (for annotation logic, which is to be moved elsewhere)

import sys

# debug print
#print(sys.argv)

import codecs, json 
import cv2
import numpy as np

# Annotation drawing function 
# TODO: remove this. It will be implemented in another interface
drawing = False # true if mouse is pressed
mode = True # if True, draw rectangle. Press 'm' to toggle to curve
ix,iy = -1,-1
Thickness = 5
# mouse callback function
White = (255,255,255)
Gray = (255//2, 255//2, 255//2)
Black = (0, 0, 0)
def draw_circle(event,x,y,flags,param):
    global ix,iy,drawing,mode, prev_x, prev_y

    if event == cv2.EVENT_LBUTTONDOWN:
        drawing = True
        ix,iy = x,y
        prev_x, prev_y = x, y

    elif event == cv2.EVENT_MOUSEMOVE:
        if drawing == True:
            Color = Black
            if mode:
                Color = White
            cv2.line(mask,(prev_x, prev_y),(x,y),Color, Thickness)
            prev_x, prev_y = x, y

    elif event == cv2.EVENT_LBUTTONUP:
        drawing = False
        Color = Black
        if mode:
            Color = White
        cv2.line(mask,(prev_x, prev_y),(x,y),Color, Thickness)


# Load annotation image from provided path
file_path = ''
if len(sys.argv) >= 2:
    file_path = sys.argv[1]
else:
    import tkinter as tk
    from tkinter import filedialog
    root = tk.Tk()
    root.withdraw()
    file_path = filedialog.askopenfilename()

img = cv2.imread(file_path) #np.zeros((512, 512), np.uint8)
S = np.shape(img)
mask = np.zeros((S[0],S[1],3), img.dtype) + 255//2
cv2.namedWindow('image')

cv2.setMouseCallback('image', draw_circle)

# Perform annotation until escape key is pressed
# TODO: remove and replace
while(1):
    display = cv2.addWeighted(mask,0.5,img,0.5,0)
    cv2.imshow('image', display)
    k = cv2.waitKey(1) & 0xFF
    if k == ord('m'):
        mode = not mode
    elif k == 27: # esc key
        break

cv2.destroyAllWindows()


#this could be the place to read in an extrenal annotation mask file

# translate format of mask
newmask = np.zeros(S[:2],dtype=np.uint8)
# wherever it is marked white (sure foreground), change mask=1
# wherever it is marked black (sure background), change mask=0
newmask[(mask[:,:,0] != 255) & (mask[:,:,0] != 0)] = 2
newmask[mask[:,:,0] == 0] = 0
newmask[mask[:,:,0] == 255] = 1

# Extract background and foreground pixels based on annotation mask values
background = img[newmask == 0][:, [2,1,0]]/255.
foreground = img[newmask == 1][:, [2,1,0]]/255.
both = img[np.logical_or(newmask == 1, newmask == 0)][:, [2,1,0]]/255.

# mu0 is the background mean color, used for despill
mu0 = np.mean(background, axis = 0)
Sigma0 = np.cov(background.T)

# foreground and total means and covariances, can be used for adjusting alpha clip and gain
mu1 = np.mean(foreground, axis = 0)
Sigma1 = np.cov(foreground.T)

mu2 = np.mean(both, axis = 0)
Sigma2 = np.cov(both.T)

# Parameterize / sort background colors by luma
# To provide parameters for capsule keyer
BackgroundLuma = np.mean(background, axis = 1)

LumaMin = np.amin(BackgroundLuma)
LumaMax = np.amax(BackgroundLuma)

SortedBackgroundIndices = np.argsort(BackgroundLuma)
SortedBackgroundLuma = BackgroundLuma[SortedBackgroundIndices]

# Debug prints
#print("Min Luma: ", LumaMin)
#print("Max Luma: ", LumaMax)
#print("Sorted Min Luma: ", SortedBackgroundLuma[0])
#print("Sorted Max Luma: ", SortedBackgroundLuma[-1])

# Weighted averages for background:
# y1: 10% percentile (by sorted index) shadows 
# y2: 20% percentile (by sorted index) midtones
# y3: 10% percentile (by sorted index) highlights
N = len(SortedBackgroundLuma)
x = np.linspace(0., 1., N)
y1 = np.zeros(N)
y2 = np.zeros(N)
y3 = np.zeros(N)
y1[x < 1./10.] = 1.
y2[np.logical_and(x > 2./5., x < 3./5.)] = 1.
y3[x > 9./10.] = 1.

Low = np.average(background[SortedBackgroundIndices], axis = 0, weights = y1)
Mid = np.average(background[SortedBackgroundIndices], axis = 0, weights = y2)
Hig = np.average(background[SortedBackgroundIndices], axis = 0, weights = y3)

# debug print
# print("Low: ", Low)
# print("Mid: ", Mid)
# print("Hig: ", Hig)

# Compute Luma, Primary Color, Secondary Color diagonal covariance scaling for background

LumaAxis = np.array([1., 1., 1.])

PrimaryAxis = mu0 - LumaAxis * np.dot(mu0, LumaAxis) / 3

SecondaryAxis = np.cross(LumaAxis, PrimaryAxis)

from numpy import linalg as LA
LumaAxis      = LumaAxis / LA.norm(LumaAxis)
PrimaryAxis   = PrimaryAxis / LA.norm(PrimaryAxis)
SecondaryAxis = SecondaryAxis / LA.norm(SecondaryAxis)

# Set parameters in model to be written to JSON file
Model = {
    "Despill" :
    {
        "DespillColor" : (mu0.tolist() + [0.0])
    },
    "CapsuleKeyer" :
    {
        "BackgroundShadows"   : (Low.tolist() + [0.0]),
        "BackgroundMidtones"  : (Mid.tolist() + [0.0]),
        "BackgroundHighlight" : (Hig.tolist() + [0.0]),
        "LumaScaling"      : 0.5, # TODO: Calculate Automatically.
        "PrimaryScaling"   : 2.0, # TODO: Calculate Automatically.
        "SecondaryScaling" : 1.0  # TODO: Calculate Automatically.
    },
    "AlphaPostProcessing" : 
    {
        "clip" : 0.1,  # TODO: Calculate Automatically
        "gain" : 2.0,  # TODO: Calculate Automatically
    }
}

if len(sys.argv) >= 3:
    filename = sys.argv[2]
else:
    from tkinter import Tk
    from tkinter.filedialog import askopenfilename
    from tkinter.filedialog import asksaveasfilename
    Tk().withdraw() # we don't want a full GUI, so keep the root window from appearing
    filename = asksaveasfilename() # show an "Open" dialog box and return the path to the selected file

# Write model to file in JSON format:
json.dump(Model, codecs.open(filename, 'w', encoding='utf-8'), separators=(',', ':'), sort_keys=True, indent=4) ### this saves the array in .json format

print("Analysis Script Completed.")


# Python JSON reference
# Decoding
"""
JSON	Python
object	dict
array	list
string	str
number (int)	int
number (real)	float
true	True
false	False
null	None
"""

# Encoding
"""
Python	JSON
dict	object
list,   tuple array
str	    string
int,    float, int- & float-derived Enums	number
True	true
False	false
None	null
"""