﻿//using System;
//using System.Collections.Generic;
//using System.IO;
//using System.Linq;
//using System.Reflection;
//using System.Text;
//using System.Threading.Tasks;
//using Microsoft.Scripting.Hosting;
//using Python.Runtime;

//namespace CCP.PythonScripting
//{
//    public sealed class PythonScripting
//    {
//        private static AssemblyLoader assemblyLoader = new AssemblyLoader();
//        public PythonScripting() {
//            string[] cmd = Environment.GetCommandLineArgs();
//            runPythonTest(cmd.Length, cmd);
//        }

//        [STAThread]
//        public static int runPythonTest(int argc, string[] argv)
//        {
//            // Only net40 is capable to safely inject python.runtime.dll into resources.
//            // reference the static assemblyLoader to stop it being optimized away
//            AssemblyLoader a = assemblyLoader;
//            int ver = Python.Runtime.Runtime.pyversionnumber;
//            string dll = Python.Runtime.Runtime.PythonDLL;
//            string vers = Python.Runtime.Runtime.pyversion;
//            string[] cmd = Environment.GetCommandLineArgs();
//            PythonEngine.Initialize();
//            int i = Runtime.Py_Main(cmd.Length, cmd);
//            PythonEngine.Shutdown();

//            return i;
//        }


//        // Register a callback function to load embedded assemblies.
//        // (Python.Runtime.dll is included as a resource)
//        private sealed class AssemblyLoader
//        {
//            private Dictionary<string, Assembly> loadedAssemblies;

//            public AssemblyLoader()
//            {
//                loadedAssemblies = new Dictionary<string, Assembly>();

//                AppDomain.CurrentDomain.AssemblyResolve += (sender, args) =>
//                {
//                    string shortName = args.Name.Split(',')[0];
//                    string resourceName = $"{shortName}.dll";

//                    if (loadedAssemblies.ContainsKey(resourceName))
//                    {
//                        return loadedAssemblies[resourceName];
//                    }

//                    // looks for the assembly from the resources and load it
//                    using (Stream stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(resourceName))
//                    {
//                        if (stream != null)
//                        {
//                            var assemblyData = new byte[stream.Length];
//                            stream.Read(assemblyData, 0, assemblyData.Length);
//                            Assembly assembly = Assembly.Load(assemblyData);
//                            loadedAssemblies[resourceName] = assembly;
//                            return assembly;
//                        }
//                    }
//                    return null;
//                };
//            }
//        }
//    }
//}
