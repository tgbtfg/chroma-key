import numpy as np
import codecs, json 

from tkinter import Tk
from tkinter.filedialog import askopenfilename
from tkinter.filedialog import asksaveasfilename

Tk().withdraw() # we don't want a full GUI, so keep the root window from appearing
filename = asksaveasfilename() # show an "Open" dialog box and return the path to the selected file
print(filename)

def arrayToFile(a, filename): 
    b = a.tolist() # nested lists with same data, indices
    json.dump(b, codecs.open(filename, 'w', encoding='utf-8'), separators=(',', ':'), sort_keys=True, indent=4) ### this saves the array in .json format

# In order to "unjsonify" the array use:
def arrayFromFile(filename):
    obj_text = codecs.open(filename, 'r', encoding='utf-8').read()
    b_new = json.loads(obj_text)
    a_new = np.array(b_new)
    return a_new

a = np.arange(10).reshape(2,5) # a 2 by 5 array

arrayToFile(a, filename)

# Decoding
"""
JSON	Python
object	dict
array	list
string	str
number (int)	int
number (real)	float
true	True
false	False
null	None
"""

# Encoding
"""
Python	JSON
dict	object
list, tuple	array
str	string
int, float, int- & float-derived Enums	number
True	true
False	false
None	null
"""