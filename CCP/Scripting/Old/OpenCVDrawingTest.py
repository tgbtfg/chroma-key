import cv2
import numpy as np

drawing = False # true if mouse is pressed
mode = True # if True, draw rectangle. Press 'm' to toggle to curve
ix,iy = -1,-1
Thickness = 5
# mouse callback function
White = (255,255,255)
Black = (255//2, 255//2, 255//2)
Black = (0, 0, 0)
def draw_circle(event,x,y,flags,param):
    global ix,iy,drawing,mode, prev_x, prev_y

    if event == cv2.EVENT_LBUTTONDOWN:
        drawing = True
        ix,iy = x,y
        prev_x, prev_y = x, y

    elif event == cv2.EVENT_MOUSEMOVE:
        if drawing == True:
            Color = Black
            if mode:
                Color = White
            cv2.line(mask,(prev_x, prev_y),(x,y),Color, Thickness)
            prev_x, prev_y = x, y

    elif event == cv2.EVENT_LBUTTONUP:
        drawing = False
        Color = Black
        if mode:
            Color = White
        cv2.line(mask,(prev_x, prev_y),(x,y),Color, Thickness)

#cv2.circle(img, center, radius, color[, thickness[, lineType[, shift]]])
#cv2.line(img, pt1, pt2, color[, thickness[, lineType[, shift]]]) 
import tkinter as tk
from tkinter import filedialog

root = tk.Tk()
root.withdraw()

file_path = filedialog.askopenfilename()

img = cv2.imread(file_path) #np.zeros((512, 512), np.uint8)
S = np.shape(img)
mask = np.zeros((S[0],S[1],3), img.dtype) + 255//2
cv2.namedWindow('image')

cv2.setMouseCallback('image',draw_circle)

while(1):
    display = cv2.addWeighted(mask,0.5,img,0.5,0)
    cv2.imshow('image', display)
    k = cv2.waitKey(1) & 0xFF
    if k == ord('m'):
        mode = not mode
    elif k == 27: # esc key
        break

cv2.destroyAllWindows()

from matplotlib import pyplot as plt

bgdModel = np.zeros((1,65),np.float64)
fgdModel = np.zeros((1,65),np.float64)

newmask = np.zeros(S[:2],dtype=np.uint8)
# wherever it is marked white (sure foreground), change mask=1
# wherever it is marked black (sure background), change mask=0
newmask[(mask[:,:,0] != 255) & (mask[:,:,0] != 0)] = 2
newmask[mask[:,:,0] == 0] = 0
newmask[mask[:,:,0] == 255] = 1
#newmask = cv2.cvtColor(newmask, cv2.CV_8UC1)
cutoutmask, bgdModel, fgdModel = cv2.grabCut(img,np.copy(newmask),None,bgdModel,fgdModel,5,cv2.GC_INIT_WITH_MASK)
cutoutmask = np.where((cutoutmask==2)|(cutoutmask==0),0,1).astype('uint8')
plt.imshow(cutoutmask, cmap='gray')
plt.figure()
plt.show()



# Note: no need to flatten if using img[newmask == k]
S = np.shape(img)
img_flat = np.reshape(img, (-1, S[2]))
newmask_flat = np.reshape(newmask, (-1))

maskedfgd = np.copy(img)
maskedfgd[cutoutmask == 0] = 0
plt.figure()
plt.imshow(maskedfgd)

maskedbgd = np.copy(img)
maskedbgd[cutoutmask != 0] = 0
plt.figure()
plt.imshow(maskedbgd)

plt.show()

masked_img = img[newmask == 0]/255
mu = np.mean(masked_img, axis = 0)
centered = masked_img - mu
Sigma = np.cov(centered.T)


from sklearn import svm

annotated = np.where(newmask == 0, 0., 1.) * np.where(newmask == 1, 1., 0.)
annotated = annotated[np.logical_or(newmask == 0, newmask == 1)]

X = img[np.logical_or(newmask == 0, newmask == 1)]
Y = annotated

from sklearn import linear_model

logreg = linear_model.LogisticRegression()
logreg.fit(X, Y)

Z = logreg.predict_proba(img_flat)[:, :1]
logregimg = np.reshape(Z, (S[0], S[1]))
plt.imshow(logregimg)


"""
clf = svm.SVC()
clf.fit(X[::100], Y[::100])

from mpl_toolkits.mplot3d import Axes3D

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
xs, ys, zs = img[newmask == 0][::100].T
xr, yr, zr = img[newmask == 1][::100].T
ax.scatter(xs, ys, zs, c='r')
ax.scatter(xr, yr, zr, c='b')
"""

from sklearn.mixture import GaussianMixture, BayesianGaussianMixture
from skimage.color import label2rgb
n_components = 16
cv_type = 'full'
verbosity = 2
gmm = GaussianMixture(n_components=n_components, covariance_type=cv_type, verbose=verbosity)
"""
gmm = BayesianGaussianMixture(
    n_components=n_components, covariance_type=cv_type, weight_concentration_prior=1e-2,
    weight_concentration_prior_type='dirichlet_process',
    mean_precision_prior=1e-2, covariance_prior=1e0 * np.eye(3),
    init_params="kmeans", max_iter=100, random_state=2, verbose=verbosity)
"""
gmm.fit(X)
pred = gmm.predict_proba(X)
pred_img_flat = pred = gmm.predict_proba(img_flat)
pred_img = np.reshape(pred_img_flat, (S[0], S[1], -1))


gmm_covariances = gmm.covariances_
gmm_means = gmm.means_

lbl = np.array([list(range(n_components))])
lblcolors = label2rgb(lbl)[0]

softseg = np.clip(np.dot(pred_img, lblcolors), 0., 1.)

#pred_img = np.concatenate((pred_img, np.array([newmask.T]).T * 0.), axis = 2)

overlay_img = (softseg + np.dot(img, np.ones((3,1))/3 )/255)/2

overlay_display = overlay_img


cv2.namedWindow('overlay')
while(1):
    cv2.imshow('overlay', overlay_display)
    k = cv2.waitKey(1) & 0xFF
    if k == ord('m'):
        if mode:
            overlay_display = overlay_img
        else:
            overlay_display = img
        mode = not mode
        
    if k == 27: # esc key
        break

cv2.destroyAllWindows()

mu0 = np.mean(img[newmask == 0], axis = 0)
Sigma0 = np.cov(img[newmask == 0].T)

mu1 = np.mean(img[newmask == 1], axis = 0)
Sigma1 = np.cov(img[newmask == 1].T)

from scipy.stats import multivariate_normal

norm0 = multivariate_normal.pdf(img_flat, mean=mu0, cov=Sigma0);
norm0_img = np.reshape(norm0, S[:2])

norm1 = multivariate_normal.pdf(img_flat, mean=mu1, cov=Sigma1);
norm1_img = np.reshape(norm1, S[:2])

normsum = norm0_img + norm1_img
normbg = norm0_img / normsum
normfg = norm1_img / normsum

def displayImg(image, name = "Image"):
    cv2.namedWindow(name)
    cv2.imshow(name, image)
    while(1):
        k = cv2.waitKey(1) & 0xFF
        if k == 27: # esc key
            break
    cv2.destroyAllWindows()


Gaussians = zip(gmm_means, gmm_covariances)
pdf_images = []
for gaussian in Gaussians:
    gaussian_pdf_flat = multivariate_normal.pdf(img_flat, mean=gaussian[0], cov=gaussian[1]);
    gaussian_pdf = np.reshape(gaussian_pdf_flat, S[:2])
    pdf_images += [gaussian_pdf.T]

pdf_images = np.array(pdf_images).T

num = np.dot(pdf_images, gmm_means)
den = np.sum(pdf_images, axis=2) * 255
multi_pdf = num / np.array([den.T]).T

displayImg(multi_pdf, name = "multi_pdf")

# Background color 'projection'
# means_proj = np.dot(gmm_means, np.array([0., 1., 0.]))
# means_proj_ranks = np.argsort(means_proj)
keying_color = np.array([0., 1., 0.])
means_dist   = np.linalg.norm(gmm_means/255 - keying_color, axis = 1)
means_dist_ranks = np.argsort(means_dist)

f, axis_list = plt.subplots(1, len(means_dist))
f.title = "Sorted cluster centers"
for i, rank in enumerate(means_dist_ranks):
   col = gmm_means[rank]
   axis_list[i].set_xticks([])
   axis_list[i].set_yticks([])
   axis_list[i].imshow([[col[[2,1,0]]/255]])

#from sklearn.feature_extraction import image
#patches = image.extract_patches_2d(img, (5, 5))


# CovF shape: 3x3
# CovB shape: 3x3
# meanF shape: 3x1
# meanB shape: 3x1
# sigma_data shape: 1
# alpha shape: 1
# PixelColor shape: 3x1
# NOTE: ERROR: assumes form where (A + B)^-1 has a special form in which B is rank 1, which is not the case, it has rank 3 in general...
def solve_bayesian_matting(CovF, CovB, meanF, meanB, alpha, sigma_data, PixelColor):
    def lerp(a, b, x):
        return (1.-x) * a + x * b
    traceF = np.trace(CovF)
    traceB = np.trace(CovB)
    invAlpha = 1.-alpha
    invAlpha2 = invAlpha ** 2
    Alpha2 = alpha ** 2
    s2_d = sigma_data **2
    T = 1. / (s2_d + Alpha2 * traceF + invAlpha2 * traceB)
    Sigma = Alpha2 * CovF + invAlpha2 * CovB
    mu = lerp(meanB, meanF, alpha)
    I = np.eye(3)
    common_part = np.dot(I - T * Sigma, PixelColor / s2_d) - T * mu
    F = meanF + alpha * np.dot(CovF, common_part)
    B = meanB + (1.-alpha) * np.dot(CovB, common_part)
    new_alpha = np.dot(PixelColor - B, F - B) / np.dot(F - B, F - B)
    return F, B, new_alpha


Bindex = means_dist_ranks[0]
Findex = means_dist_ranks[9]

meanB = gmm_means[Bindex] / 255
meanF = gmm_means[Findex] / 255

CovB = gmm_covariances[Bindex] / 255 / 255
CovF = gmm_covariances[Findex] / 255 / 255

PixelColor = gmm_means[means_dist_ranks[6]] / 255
sigma = 1.
alpha = 0.5
bayesian_sol = solve_bayesian_matting(CovF, CovB, meanF, meanB, alpha, sigma, PixelColor)

from numpy.linalg import inv, solve, matrix_rank
def bayesian_matting_solve_reference(CovF, CovB, meanF, meanB, alpha, sigma, PixelColor):
    C = np.reshape(PixelColor, (3, 1))
    invF = inv(CovF)
    invB = inv(CovB)
    I_sigma = np.eye(3) / (sigma**2)
    M = np.block([
        [invF + (alpha**2)  * I_sigma,  alpha*(1.-alpha) * I_sigma],
        [alpha*(1.-alpha) * I_sigma, invB + (1.-alpha)**2 * I_sigma]
    ])

    muF = np.reshape(meanF, (3, 1))
    muB = np.reshape(meanB, (3, 1))

    y = np.block([
        [np.dot(invF, muF) + alpha**2 * C], 
        [np.dot(invB, muB) + (1.-alpha)**2  * C]
    ])
    X = solve(M, y)
    F = X[:3]
    B = X[3:]
    new_alpha = np.dot(PixelColor - B.T, F - B) / np.dot(F.T - B.T, F - B)
    return F, B, new_alpha

ref_sol = bayesian_matting_solve_reference(CovF, CovB, meanF, meanB, alpha, sigma, PixelColor)

def solve_bayesian_matting2(CovF, CovB, meanF, meanB, alpha, sigma, PixelColor, iterations=3, log = False):
    def lerp(a, b, x):
        return (1.-x) * a + x * b
    F_i = meanF
    B_i = meanB

    C = PixelColor
    alpha_i = alpha
    
    S = np.linspace(1., sigma, iterations)
    for iteration in range(iterations):
        s = S[iteration]
        Offset = C - lerp(B_i, F_i, alpha_i) # Vector from alpha mixed color between background and foreground and observed color
        F_i = np.clip(meanF + alpha_i * np.dot(CovF, Offset) / s, 0., 1.)
        B_i = np.clip(meanB + (1.-alpha_i) * np.dot(CovB, Offset) / s, 0., 1.)
        FB = F_i - B_i
        alpha_i = np.clip(np.dot(PixelColor - B_i, FB) / np.dot(FB, FB), 0., 1.)
        if log:
            print(F_i, B_i, alpha_i, ". Error dist =", np.linalg.norm(C - lerp(B_i, F_i, alpha_i)))
    return F_i, B_i, alpha_i

bayesian_sol2 = solve_bayesian_matting2(CovF, CovB, meanF, meanB, alpha, sigma, PixelColor)

cF = Sigma1/255/255
cB = Sigma0/255/255
muF = mu1/255
muB = mu0/255
matte_img = img[::10, ::10]
bayesian_matte = [ solve_bayesian_matting2(cF, cB, muF, muB, 0.5, 0.01, C/255, 3) for C in np.reshape(img[::10, ::10], (-1, 3))]


bayesian_matte_alpha = np.array([x[2] for x in bayesian_matte])
bayesian_matte_alpha_img = np.reshape(bayesian_matte_alpha, np.shape(matte_img)[:2])
plt.imshow(bayesian_matte_alpha_img, cmap='gray')