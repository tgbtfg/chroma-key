﻿print("Testing numpy")
import numpy as np

x = np.linspace(0., 10., 100)
y = np.sin(x)
print(y)

print("Testing matplotlib")
import matplotlib
matplotlib.use('TkAgg') # matplotlib backend Qt5Agg crashes somewhere in QtCore module
print(matplotlib.get_backend())
import matplotlib.pyplot as plt
plt.plot(x, y)
plt.show()

input(">>")
print("Testing opencv")
import cv2
img = cv2.imread('C:\\Users\\Admin\\Desktop\\Premultiplied Recombine Tests\\GammaSpacePremultiplied.PNG')
print(img)
cv2.imshow('opencv image', img)
cv2.waitKey()
input(">>")

print("Testion json")
import json
print(json.dumps(['foo', {'bar': ('baz', None, 1.0, 2)}]))

print("Test misc modules")
#from scipy.sparse import lil_matrix
#from scipy.sparse.linalg import spsolve
#from scipy.spatial import ConvexHull
#from sklearn import cluster
#from sklearn import mixture
from scipy import ndimage
from sklearn.feature_extraction import image
from sklearn.linear_model import Lasso
from sklearn.linear_model import Ridge
from mpl_toolkits.mplot3d import Axes3D
print("ok")
while True:
    print("Hello World!")
    input(">>")