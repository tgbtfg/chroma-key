import sys
print(sys.argv)

import codecs, json 
import cv2
import numpy as np

drawing = False # true if mouse is pressed
mode = True # if True, draw rectangle. Press 'm' to toggle to curve
ix,iy = -1,-1
Thickness = 5
# mouse callback function
White = (255,255,255)
Gray = (255//2, 255//2, 255//2)
Black = (0, 0, 0)
def draw_circle(event,x,y,flags,param):
    global ix,iy,drawing,mode, prev_x, prev_y

    if event == cv2.EVENT_LBUTTONDOWN:
        drawing = True
        ix,iy = x,y
        prev_x, prev_y = x, y

    elif event == cv2.EVENT_MOUSEMOVE:
        if drawing == True:
            Color = Black
            if mode:
                Color = White
            cv2.line(mask,(prev_x, prev_y),(x,y),Color, Thickness)
            prev_x, prev_y = x, y

    elif event == cv2.EVENT_LBUTTONUP:
        drawing = False
        Color = Black
        if mode:
            Color = White
        cv2.line(mask,(prev_x, prev_y),(x,y),Color, Thickness)

#cv2.circle(img, center, radius, color[, thickness[, lineType[, shift]]])
#cv2.line(img, pt1, pt2, color[, thickness[, lineType[, shift]]]) 
import tkinter as tk
from tkinter import filedialog

root = tk.Tk()
root.withdraw()

file_path = ''
if len(sys.argv) >= 2:
    file_path = sys.argv[1]
else:
    file_path = filedialog.askopenfilename()

img = cv2.imread(file_path) #np.zeros((512, 512), np.uint8)
S = np.shape(img)
mask = np.zeros((S[0],S[1],3), img.dtype) + 255//2
cv2.namedWindow('image')

cv2.setMouseCallback('image', draw_circle)

while(1):
    display = cv2.addWeighted(mask,0.5,img,0.5,0)
    cv2.imshow('image', display)
    k = cv2.waitKey(1) & 0xFF
    if k == ord('m'):
        mode = not mode
    elif k == 27: # esc key
        break

cv2.destroyAllWindows()

newmask = np.zeros(S[:2],dtype=np.uint8)
# wherever it is marked white (sure foreground), change mask=1
# wherever it is marked black (sure background), change mask=0
newmask[(mask[:,:,0] != 255) & (mask[:,:,0] != 0)] = 2
newmask[mask[:,:,0] == 0] = 0
newmask[mask[:,:,0] == 255] = 1

background = img[newmask == 0][:, [2,1,0]]/255.
foreground = img[newmask == 1][:, [2,1,0]]/255.
both = img[np.logical_or(newmask == 1, newmask == 0)][:, [2,1,0]]/255.

mu0 = np.mean(background, axis = 0)
Sigma0 = np.cov(background.T)

mu1 = np.mean(foreground, axis = 0)
Sigma1 = np.cov(foreground.T)

mu2 = np.mean(both, axis = 0)
Sigma2 = np.cov(both.T)


Model = {
    "background" :
    {
        "mean" : str(mu0.tolist()),
        "cov" : str(Sigma0.tolist()),
    },
    "foreground" :
    {
        "mean" : str(mu1.tolist()),
        "cov" : str(Sigma1.tolist()),
    },
    "both" :
    {
        "mean" : str(mu2.tolist()),
        "cov" : str(Sigma2.tolist()),
    }
}

from tkinter import Tk
from tkinter.filedialog import askopenfilename
from tkinter.filedialog import asksaveasfilename

Tk().withdraw() # we don't want a full GUI, so keep the root window from appearing
filename = asksaveasfilename() # show an "Open" dialog box and return the path to the selected file
print(filename)

json.dump(Model, codecs.open(filename, 'w', encoding='utf-8'), separators=(',', ':'), sort_keys=True, indent=4) ### this saves the array in .json format


def arrayToFile(a, filename): 
    b = a.tolist() # nested lists with same data, indices
    json.dump(b, codecs.open(filename, 'w', encoding='utf-8'), separators=(',', ':'), sort_keys=True, indent=4) ### this saves the array in .json format

# In order to "unjsonify" the array use:
def arrayFromFile(filename):
    obj_text = codecs.open(filename, 'r', encoding='utf-8').read()
    b_new = json.loads(obj_text)
    a_new = np.array(b_new)
    return a_new

#a = np.arange(10).reshape(2,5) # a 2 by 5 array
#arrayToFile(a, filename)

# Decoding
"""
JSON	Python
object	dict
array	list
string	str
number (int)	int
number (real)	float
true	True
false	False
null	None
"""

# Encoding
"""
Python	JSON
dict	object
list, tuple	array
str	string
int, float, int- & float-derived Enums	number
True	true
False	false
None	null
"""