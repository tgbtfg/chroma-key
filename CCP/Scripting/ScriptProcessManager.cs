﻿using System;
using System.Diagnostics; // Process, etc
using System.IO;
using System.Linq;
using Microsoft.Win32;

namespace CCP.Scripting
{
    class ScriptProcessManager
    {
        private Process process;

        public string InterpreterPath;
        public string ScriptPath;
        public string ScriptArguments = "";
        public string WorkingDirectory = "";
        
        public event EventHandler ScriptExecutionStarted;
        public event EventHandler ScriptExecutionStopped;


        public ScriptProcessManager() {}

        // System.Environment
        // System.IO.Path
        // System.IO.Directory
        // System.IO.File
        // Microsoft.Win32.Registry

        // TODO: Use memory mapped files for communicating with child processes
        // see:
        // https://docs.microsoft.com/en-us/dotnet/standard/io/memory-mapped-files
        // https://stackoverflow.com/questions/4291912/process-start-how-to-get-the-output
        // And for python child process:
        // https://docs.python.org/2/library/mmap.html

        public bool IsScriptRunning()
        {
            if (process != null) {
                try {
                    Process[] processesFound = Process.GetProcessesByName(process.ProcessName);
                    foreach(Process p in processesFound)
                    {
                        if (p.Id == process.Id) return true;
                    }
                }
                catch (InvalidOperationException) {
                    // Error message
                    return false;
                }
            }
            return false;
        }

        public void RunScript()
        {
            bool InterpreterDetected = DetectInterpreter();
            bool ScriptNotRunning    = !IsScriptRunning();
            if (InterpreterDetected && ScriptNotRunning)
            {
                LaunchProcess();
            } 
            else
            {
                // error
            }
        }

        internal void TerminateProcess()
        {
            if(IsScriptRunning())
                process.Kill();
        }

        public bool LaunchProcess() {
            bool ScriptFound = File.Exists(ScriptPath);
            bool InterpreterFound = File.Exists(InterpreterPath);
            if (ScriptFound && InterpreterFound)
            {
                string ScriptString = string.Format("{0} {1}", ScriptPath, ScriptArguments);
                process = RunCommand(InterpreterPath, "-u", ScriptString, WorkingDirectory);
                if (process != null) return true;
            }
            else
            {
                Debug.WriteLine("Cannot launch process");
                Debug.WriteLine("Script " + (ScriptFound ? "found." : "not found."));
                Debug.WriteLine("Interpreter " + (InterpreterFound ? "found." : "not found."));
                // error
            }
            return false;
        }

        public bool DetectInterpreter()
        {
            string subkey = @"SOFTWARE\Python\PythonCore";

            RegistryKey interpreterKey = Registry.CurrentUser.OpenSubKey(subkey);
            foreach (string tag in interpreterKey.GetSubKeyNames())
            {
                double interpreterVersion = Convert.ToDouble(tag.Split('-').First());
                if(CheckInterpreterVersion(interpreterVersion))
                {
                    RegistryKey interpreterRegistryKey = interpreterKey.OpenSubKey(tag + "\\InstallPath");
                    Object registryObject = interpreterRegistryKey.GetValue("ExecutablePath");
                    if(registryObject != null)
                    {
                        //Properties.Settings.Default.python3ExePath = (string)registryObject;
                        //Properties.Settings.Default.Save();
                        return true;
                    }
                }
            }
            return false;
        }

        private bool CheckInterpreterVersion(double interpreterVersion)
        {
            return true; // TODO implement
        }

        private Process RunCommand(string executable, string cmd, string args, string workingDir)
        {
            ProcessStartInfo startInfo = new ProcessStartInfo()
            {
                FileName = executable,
                WorkingDirectory = workingDir,
                Arguments = string.Format("{0} {1}", cmd, args),
                CreateNoWindow = false,
                UseShellExecute = false,
                RedirectStandardInput  = false,
                RedirectStandardError  = true,
                RedirectStandardOutput = true,
                WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden,
            };
            
            try {
                Process p = new Process()
                {
                    StartInfo = startInfo,
                    EnableRaisingEvents = true
                };

                // Subscribe to process events
                p.Exited += OnProcessExited;
                p.ErrorDataReceived += OnErrorDataReceived;
                p.OutputDataReceived += OnOutputDataReceived;

                // Begin execution
                p.Start();
                p.BeginOutputReadLine();
                p.BeginErrorReadLine();
                this.ScriptExecutionStarted?.Invoke(this, new EventArgs());

                return p;
            }
            catch(Exception)
            {
                // Error, print error message
                // display ex.Message
                return null;
            }
        }

        private void OnOutputDataReceived(object sender, DataReceivedEventArgs eventArgs)
        {
            // TODO handle data received
            Console.WriteLine(eventArgs.Data);
        }

        private void OnErrorDataReceived(object sender, DataReceivedEventArgs eventArgs)
        {
            // TODO handle error received
            Console.WriteLine(eventArgs.Data);
        }

        private void OnProcessExited(object sender, EventArgs eventArgs)
        {
            // Log script execution stopped
            Console.WriteLine("Script process exited");
            ScriptExecutionStopped?.Invoke(this, eventArgs);
        }
    }
}
