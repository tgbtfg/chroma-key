﻿using Microsoft.Win32;
using System;
using System.IO;
using System.Windows;
using System.Windows.Controls;

namespace CCP.Scripting
{
    /// <summary>
    /// Interaction logic for ScriptLauncherControl.xaml
    /// </summary>
    public partial class ScriptLauncherControl : UserControl
    {
        private ScriptProcessManager scriptManager;


        public string inputPath  = "";
        public string outputPath = "";

        public ScriptLauncherControl()
        {
            scriptManager = new ScriptProcessManager();
            scriptManager.ScriptExecutionStopped +=
                (o, e) =>
                {
                    // Let application dispatcher perform task, so
                    // to not access button object from non-owning thread
                    Application.Current.Dispatcher.Invoke((Action)(() =>
                    {
                        StartButton.IsEnabled = true;
                        StopButton.IsEnabled  = false;
                    }));
                };
            
            scriptManager.ScriptExecutionStarted +=
                (o, e) =>
                {
                    Application.Current.Dispatcher.Invoke((Action)(() =>
                    {
                        StartButton.IsEnabled = false;
                        StopButton.IsEnabled  = true;
                    }));
                };

            InitializeComponent();
        }

        public void RunScript()
        {
            bool validScript = true;
            scriptManager.InterpreterPath = @"D:\Anaconda3\python.exe";
            scriptManager.ScriptArguments = string.Format("{0} {1}", inputPath, outputPath);
            if (validScript)
            {
                if (scriptManager.LaunchProcess())
                {
                    StartButton.IsEnabled = false;
                    StopButton.IsEnabled = true;
                }
            }
        }

        private void StartButton_Click(object sender, RoutedEventArgs e)
        {
            RunScript();
            //"D:\\CCP\\CCP\\CCP\\bin\\Debug\\HelloWorld.py"
            //scriptManager.SetScript(@"D:\CCP\CCP\CCP\Scripting\HelloWorld.py");

        }

        private void StopButton_Click(object sender, RoutedEventArgs e)
        {
            if (scriptManager.IsScriptRunning())
            {
                scriptManager.TerminateProcess();
                StartButton.IsEnabled = true;
                StopButton.IsEnabled = false;
            }
        }

        private bool BrowseForScript()
        {
            OpenFileDialog browseScriptFileDialog = new OpenFileDialog()
            {
                Title = "Find python script",
                Filter = "python scripts (*.py)|*.py|All files (*.*)|*.*",
                FilterIndex = 0,
                RestoreDirectory = true
            };

            if (((scriptManager.ScriptPath) ?? "") == "")
            {
                browseScriptFileDialog.InitialDirectory = Directory.GetCurrentDirectory();
            }
            else
            {
                browseScriptFileDialog.InitialDirectory = Path.GetDirectoryName(scriptManager.ScriptPath);
            }
            if (browseScriptFileDialog.ShowDialog() == true)
            {
                string scriptPath = browseScriptFileDialog.FileName;
                //scriptManager.ScriptPath = scriptPath;
                //ScriptSelectionBox.Text = Path.GetFileName(scriptPath);
                SetScriptPath(scriptPath);
                return true;
            }
            return false;
        }

        public void SetScriptPath(string scriptPath)
        {
            scriptManager.ScriptPath = scriptPath;
            ScriptSelectionBox.Text = Path.GetFileName(scriptPath);
        }

        private bool BrowseForInterpreter()
        {
            OpenFileDialog interpreterFileDialog = new OpenFileDialog { Title = "Find python executable" };
            if (scriptManager.ScriptPath == "")
            {
                interpreterFileDialog.InitialDirectory = "c:\\"; // Main disk not neccessarily C:?
            }
            else
            {
                interpreterFileDialog.InitialDirectory = Path.GetDirectoryName(scriptManager.ScriptPath);
            }

            interpreterFileDialog.Filter = "Python exe (python.exe)|python.exe|All files (*.*)|*.*";
            interpreterFileDialog.FilterIndex = 2;
            interpreterFileDialog.RestoreDirectory = true;

            if (interpreterFileDialog.ShowDialog() == true)
            {
                scriptManager.ScriptPath = interpreterFileDialog.FileName;
                return true;
            }

            return false;
        }

        private void BrowseScriptButton_Click(object sender, RoutedEventArgs e)
        {
            BrowseForScript();
        }

        private void InputSelectionBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            inputPath = InputSelectionBox.Text;
        }
    }
}
