﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Collections.ObjectModel;

namespace CCP
{
    /// <summary>
    /// Interaction logic for FormCreationDialogWindow.xaml
    /// </summary>
    public partial class FormDialogWindow : Window
    {
        IDictionary<string, TextBox> textFields = new Dictionary<string, TextBox>();
        IDictionary<string, ComboBox> selectionFields = new Dictionary<string, ComboBox>();

        public FormDialogWindow()
        {
            InitializeComponent();
        }
        
        private void ButtonDialogOk_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void Window_ContentRendered(object sender, EventArgs e)
        {
            //txtAnswer.SelectAll();
            //txtAnswer.Focus();
        }
        
        public IDictionary<string, string> Answers
        {
            get {
                IDictionary<string, string> answers = new Dictionary<string, string>();

                foreach (KeyValuePair<string, TextBox> field in textFields) answers.Add(field.Key, field.Value.Text);
                foreach (KeyValuePair<string, ComboBox> field in selectionFields) answers.Add(field.Key, field.Value.SelectedValue?.ToString());

                return answers;
            }
        }

        internal void AddField(string label = "Name", string defaultInput = "")
        {
            TextBox FieldInputBox = new TextBox { MinWidth = 250, TextAlignment = TextAlignment.Left, Text=defaultInput
            };
            VerticalAlignment = VerticalAlignment.Center;

            Label FieldLabel = new Label() { Content = label };

            InputGrid.RowDefinitions.Add(new RowDefinition());
            InputGrid.Children.Add(FieldLabel);
            Grid.SetRow(FieldLabel, InputGrid.RowDefinitions.Count - 2);
            Grid.SetColumn(FieldLabel, 0);

            InputGrid.Children.Add(FieldInputBox);
            Grid.SetRow(FieldInputBox, InputGrid.RowDefinitions.Count - 2);
            Grid.SetColumn(FieldInputBox, 1);

            Grid.SetRow(ConfirmationBox, InputGrid.RowDefinitions.Count - 1);

            textFields[label] = FieldInputBox;

            FieldLabel.Content = label;
        }

        internal void AddComboBox(string label, List<string> alternatives)
        {
            ComboBox Box = new ComboBox
            {
                VerticalAlignment = VerticalAlignment.Center,
                ItemsSource = new ObservableCollection<string>(alternatives),
                SelectedIndex = 0
            };
            

            Label FieldLabel = new Label() { Content = label };

            InputGrid.RowDefinitions.Add(new RowDefinition());
            InputGrid.Children.Add(FieldLabel);
            Grid.SetRow(FieldLabel, InputGrid.RowDefinitions.Count - 2);
            Grid.SetColumn(FieldLabel, 0);

            InputGrid.Children.Add(Box);
            Grid.SetRow(Box, InputGrid.RowDefinitions.Count - 2);
            Grid.SetColumn(Box, 1);

            Grid.SetRow(ConfirmationBox, InputGrid.RowDefinitions.Count - 1);

            selectionFields[label] = Box;

            FieldLabel.Content = label;
        }
    }
}
