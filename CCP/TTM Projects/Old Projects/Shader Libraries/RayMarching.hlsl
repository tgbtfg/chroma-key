RWTexture2D<unorm float4> gOutput : register(u0);

SamplerState LinearSampler
{
	Filter = MIN_MAG_MIP_LINEAR;
	AddressU = MIRROR;
	AddressV = MIRROR;
};

float4 SampleLinear(Texture2D<float4> Tex, float2 uv)
{
	return Tex.SampleLevel(LinearSampler, uv, 0.);
}

uint2 XYZtoUV(Texture2D<float4> LUT, uint N, float3 xyz)
{
	uint2 LUTDimensions = 0;
	LUT.GetDimensions(LUTDimensions.x, LUTDimensions.y);

	uint2 uv = uint2(xyz.xy * float(N - 1));
	uint w = uint(xyz.z * float(N - 1));
	uint M = (LUTDimensions.x / N);
	return uv + uint2(w % M, w / M) * N;
}


float3 ProjectToAABB(float3 p, float3 b)
{
	return clamp(p, -b, b);
}

float3 AABBCoordinate(float3 p, float3 b)
{
	return (clamp(p, -b, b) + b) / (2. * b);
}

float4 SampleSDF(Texture2D<float4> SDF, float3 AABBExtents, float3 pos)
{
	float4 Sample = SDF[XYZtoUV(SDF, 256, AABBCoordinate(pos, AABBExtents))];
	float3 P = 2. * (Sample.rgb - AABBExtents) * AABBExtents;
	float a = max(length(pos - ProjectToAABB(pos, AABBExtents)), Sample.a); // length(pos - P);
	return float4(Sample.rgb, a);
	//return SDF[XYZtoUV(SDF, 256, AABBCoordinate(pos, AABBExtents))];
}

float4 ColorCube(float3 p, float3 AABBExtents)
{
	float3 P = ProjectToAABB(p, AABBExtents);
	float3 uvw = AABBCoordinate(p, AABBExtents);
	float a = length(p - P);
	return float4(uvw, a);
}


float4 ColorCubeWireframe(float3 p, float3 AABBExtents)
{
	float4 Cube = SampleBox(p, AABBExtents);
	float e = 0.01;	// Edge thickness
	float h = .5;		// Halfwidth
	float Hole1 = sdBox(p, AABBExtents - e * float3(-1., 1., 1.));
	Cube.a = max(-Hole1, Cube.a);

	float Hole2 = sdBox(p, AABBExtents - e * float3(1., -1., 1.));
	Cube.a = max(-Hole2, Cube.a);

	float Hole3 = sdBox(p, AABBExtents - e * float3(1., 1., -1.));
	Cube.a = max(-Hole3, Cube.a);

	return Cube;
}

float4 SDFRayMarch(int2 p)
{
	float4 Color = 0.;

	float2 uv = (float2(p.xy) + .5) / float2(wh);
	float3 ro = float3(0.0, 0.0, -1.5);

	float2 aspectRatio = float2(wh.xy) / float2(wh.yy);
	float3 screen = float3(aspectRatio * (uv - .5), ro.z + 1.);

	float3 rd = normalize(screen - ro);
	int it = 30;

	float3 AABBExtents = float3(0.5, 0.5, 0.5) * 1.;

	float fieldDist = 9999999.;
	float3 fieldPos = ro;
	float4 fieldColor = 0.;

	float boxDist = 99999999.;
	float3 boxPos = ro;
	float4 boxColor = 0.;
	float3 bx = ro;

	for (int i = 0; i < it; i++)
	{
		// Trace distance field
		float3 Rotated = mul(rotY, ro);

		float4 Sample = SampleSDF(SDF, AABBExtents, Rotated);
		float d = Sample.a;
		if (d < fieldDist)
		{
			fieldDist = d;
			fieldPos = ro;
			fieldColor = Sample.rgba;
		}
		ro += d * rd;

		// Separate box tracing
		float3 rotbx = mul(rotY, bx);
		float4 BoxSample = SampleBoxWireframe(rotbx, AABBExtents);
		float t = BoxSample.a;
		if (t < boxDist)
		{
			boxDist = t;
			boxPos = bx;
			boxColor = SampleLinear(inputNode, AABBCoordinate(rotbx, AABBExtents).xy);
			boxColor = ForegroundLUT[XYZtoUV(SDF, 256, AABBCoordinate(rotbx, AABBExtents))];
			boxColor.rgb = AABBCoordinate(rotbx, AABBExtents);
		}
		bx += rd * t;
	}

	float fieldThreshold = 1. / 128. * ((sin(2. * 6.283 * time) + 1.5) * 2.);
	float boxThreshold = 1. / 256.;

	Color = (boxDist < boxThreshold || fieldDist < fieldThreshold) ?
		(
			0.5 * (boxDist   < boxThreshold && fieldDist >= fieldThreshold ? boxColor : 0.) +
			1.0 * (fieldDist < fieldThreshold ? fieldColor : 0.)
			) : inputNode[p.xy]
		;

	Color.a = time;
	return Color;
}
