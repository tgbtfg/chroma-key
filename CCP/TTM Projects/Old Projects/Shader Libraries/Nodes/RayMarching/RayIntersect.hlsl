// Box Intersection
// Calcs intersection and exit distances, and normal at intersection
//
// The box is axis aligned and at the origin, but has any size.
// For arbirarily oriented and located boxes, simply transform
// the ray accordingly and reverse the transformation for the
// returning normal(without translation)
//
float2 boxIntersection( float3 ro, float3 rd, float3 boxSize) 
{
    float3 m = 1.0/rd;
    float3 n = m*ro;
    float3 k = abs(m)*boxSize;
	
    float3 t1 = -n - k;
    float3 t2 = -n + k;

    float tN = max( max( t1.x, t1.y ), t1.z );
    float tF = min( min( t2.x, t2.y ), t2.z );
	
    if( tN > tF || tF < 0.0) return -1.0; // no intersection
    
    float3 outNormal = -sign(rd)*step(t1.yzx,t1.xyz)*step(t1.zxy,t1.xyz);

    return float2( tN, tF );
}