RWTexture2D<unorm float4> gOutput : register(u0);

[numthreads(32, 32, 1)]
void main(int3 p : SV_DispatchThreadID)
{
	gOutput[p.xy] = ( 
		inputNode[p.xy + int2(1, 0)] - inputNode[p.xy + int2(-1, 0)] +
		inputNode[p.xy + int2(1, 0)] - inputNode[p.xy + int2(-1, 0)] +
		inputNode[p.xy + int2(1, 0)] - inputNode[p.xy + int2(-1, 0)]
		)/3. + .5;
}
