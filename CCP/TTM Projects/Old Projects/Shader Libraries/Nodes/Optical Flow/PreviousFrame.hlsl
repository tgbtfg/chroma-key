RWTexture2D<unorm float4> gOutput : register(u0);

[numthreads(32, 32, 1)]
void main(int3 p : SV_DispatchThreadID)
{
	float4 EvenFrame = EvenFrames[p.xy];
	float4 OddFrame = OddFrames[p.xy];
	
	float4 PrevFrame = (EvenFrame.a > .5) ? EvenFrame : OddFrame;
	float4 CurrFrame = inputNode[p.xy];
	
	float4 Diff = CurrFrame - PrevFrame;
	gOutput[p.xy] = PrevFrame;
}
