RWTexture2D<unorm float4> gOutput : register(u0);


float3 makeNormal(float2 normalized2Ddir)
{
    float3 normalcolor = 0.;
    normalcolor.rg = normalized2Ddir;
    normalcolor.b = sqrt(1.-clamp(dot(normalized2Ddir, normalized2Ddir.rg), 0., 1.));
    return normalize(normalcolor);
}

//[-1., 1] -> [0., 1.]
float3 encodeNormal(float3 normalDir)
{
    float3 normalcolor = 0.;
    normalcolor = (normalDir + 1.) / 2.;
    return clamp(normalcolor, 0., 1.);
}

// [0., 1.] -> [-1., 1]
float3 decodeNormal(float3 normalcolor)
{
    float3 normalvector = float3(normalcolor.rg * 2. - 1., normalcolor.b);
    normalvector = normalcolor * 2. - 1.;
    return normalvector;
}

float4 ddx(int2 p) { return (XDerivative[p.xy] - .5) * 1.; }
float4 ddy(int2 p) { return (YDerivative[p.xy] - .5) * 1.; }
float4 ddt(int2 p) { return (TDerivative[p.xy] - .5) * 1.; }

float NormalDistribution(float mean, float std, float x)
{
	float a = (x - mean) / std;
	float sqrt2pi = 2.506628274631;
	return exp(-.5 * a * a ) / (std * sqrt2pi);
}

float2x2 Invert2x2(float2x2 M)
{
	float det = determinant(M);
	float2x2 InvM = {M._22, -M._12, -M._21, M._11};
	return InvM / det;
}

[numthreads(32, 32, 1)]
void main(int3 p : SV_DispatchThreadID)
{
	float2x2 A = { 0., 0., 0., 0. };
	float2 b = 0.; 
	
	int r = 5;
	for(int i = -r; i <= r; i++)
	{
		for(int j = -r; j <= r; j++)
		{
			int2 offset = int2(i, j);
				
			float3 Ix = ddx(p.xy + offset);
			float3 Iy = ddy(p.xy + offset);
			
			// ref wiki Jacobian
			float2x3 JacobianT = { Ix , Iy };
			float3x2 Jacobian = transpose(JacobianT);
			float2x2 Structural = mul(JacobianT, Jacobian);
			
			float3 It = ddt(p.xy + offset);
			
			float w = NormalDistribution(0., 1., length(float2(offset))/sqrt(float(2.*r)) );
			
			A += w * Structural;
			
			b += -w * mul(JacobianT, It);
		}
	}
	
	float2x2 InvA = Invert2x2(A);
	
	float2 velocity = mul(InvA, b);
	
	float3 normal = makeNormal(velocity);
	float3 prevNormal = decodeNormal(gOutput[p.xy].rgb);
	float3 CombinedNormal = encodeNormal(normalize(lerp(normal, prevNormal, 0.9)));

	gOutput[p.xy] = float4(CombinedNormal, 1.);
}
