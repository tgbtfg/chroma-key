RWTexture2D<unorm float4> gOutput : register(u0);

[numthreads(32, 32, 1)]
void main(int3 p : SV_DispatchThreadID)
{
		gOutput[p.xy] = ( 
		inputNode[p.xy + int2(0, 1)] - inputNode[p.xy + int2(0, -1)] +
		inputNode[p.xy + int2(0, 1)] - inputNode[p.xy + int2(0, -1)] +
		inputNode[p.xy + int2(0, 1)] - inputNode[p.xy + int2(0, -1)]
		)/3. + .5;
}
