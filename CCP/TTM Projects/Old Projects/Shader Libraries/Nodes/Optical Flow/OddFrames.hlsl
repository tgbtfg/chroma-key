RWTexture2D<unorm float4> gOutput : register(u0);

[numthreads(32, 32, 1)]
void main(int3 p : SV_DispatchThreadID)
{
	float4 PrevFrame = gOutput[p.xy];
	float4 CurrFrame = inputNode[p.xy];
	float4 NextFrame = 0.;
	if(PrevFrame.a > .5)
	{
		NextFrame = PrevFrame;
		NextFrame.a = 0;
	}
	else
	{
		NextFrame = CurrFrame;
		NextFrame.a = 1.;
	}
	gOutput[p.xy] = NextFrame;
}
