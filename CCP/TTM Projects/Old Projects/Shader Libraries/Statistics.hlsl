float NormalDistribution(float mean, float std, float x)
{
	float a = (x - mean) / std;
	float sqrt2pi = 2.506628274631;
	return exp(-.5 * a * a ) / (std * sqrt2pi);
}

float  MahalanobisDistance3D(float3 mean, float3x3 InvSigma, float3 x)
{
	float3 y = x - mean;
	return sqrt(mul(mul(y, InvSigma), y));
}

/*
float MultivariateNormalDistribution3D(float3 mean, float3x3 InvSigma, float3 x)
{
	float3 y = x - mean;
	float a = mul(mul(y, InvSigma), y);
	float tauCubed = 186.03766;
	return exp(-.5 * a * a ) / sqrt(tauCubed / determinant(InvSigma));
}
*/


float MultivariateNormalDistribution3D(float3 mean, float3x3 Sigma, float3 x)
{
	float D11 = determinant(float2x2(Sigma._22_23_23_33)); 
	float D12 = determinant(float2x2(Sigma._12_23_31_33));
	float D13 = determinant(float2x2(Sigma._12_22_13_23));

	float D22 = determinant(float2x2(Sigma._11_13_31_33));
	float D23 = determinant(float2x2(Sigma._11_12_31_23));

	float D33 = determinant(float2x2(Sigma._11_12_21_22));

	float3 abc = float3(D11, -D12, D13);
	float3 def = float3(D22, -D23, D33);
	
	float det = abc * Sigma._11_12_13;

	abc /= det;
	def /= det;

	float3x3 InvSigma = {
		abc, 
		abc.y, def.xy, 
		abc.z, def.yz 
		};

	float3 y = x - mean;
	float A = mul(mul(y, InvSigma), y);
	float tauCubed = 186.03766;
	return exp(-.5 * A * A ) / sqrt(tauCubed * det);
}