SamplerState LinearSampler
{
    Filter = MIN_MAG_MIP_LINEAR;
    AddressU = MIRROR;
    AddressV = MIRROR;
};

float2 GetUV(int2 p, int2 Size)
{
	return (float2(p)+.5) / float2(Size);
}

float4 SampleLinear(Texture2D<float4> Tex, float2 uv)
{
	return Tex.SampleLevel(LinearSampler, uv, 0.);
}

float4 Downsample(Texture2D<float4> INPUT, Texture2D<float4> OUTPUT, SamplerState Sampler, int2 p, int2 SampleDim)
{
	int2 I = 0;
	int2 O = 0;
	
	INPUT.GetDimensions(I.x, I.y);
	OUTPUT.GetDimensions(O.x, O.y);

	float4 Color = 0.;
	
	for(int i = 0; i < SampleDim.x; i++)
	{
		for(int j = 0; j < SampleDim.y; j++)
		{
			float2 offset = (float2(i, j) + .5) / float2(SampleDim);
			float2 uv = (float2(p.xy) + offset) / float2(O);
			Color += INPUT.SampleLevel(Sampler, uv, 0.);
		}
	}
	
	return Color / (SampleDim.x*SampleDim.y);
}