float3x3 SymmetricInverse(float3x3 M)
{
	float D11 = determinant(float2x2(M._22_23_23_33)); 
	float D12 = determinant(float2x2(M._12_23_31_33));
	float D13 = determinant(float2x2(M._12_22_13_23));

	float D22 = determinant(float2x2(M._11_13_31_33));
	float D23 = determinant(float2x2(M._11_12_31_23));

	float D33 = determinant(float2x2(M._11_12_21_22));

	float3 abc = float3(D11, -D12, D13);
	float3 def = float3(D22, -D23, D33);
	
	float det = abc * M._11_12_13;

	abc /= det;
	def /= det;

	float3x3 InvM = {
		abc, 
		abc.y, def.xy, 
		abc.z, def.yz 
		};
	return InvM;
}