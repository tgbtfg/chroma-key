﻿using System;
using System.ComponentModel;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;


using OpenCvSharp;

namespace CCP
{
    /// <summary>
    /// Interaction logic for ImageSlot.xaml
    /// </summary>
    public partial class ImageSlot : UserControl, INotifyPropertyChanged
    {
        public ImageSlot()
        {
            InitializeComponent();
        }
        
        public ImageSource Source
        {
            get { return ImagePane.Source; }
            set
            {
                if (ImagePane.Source != value)
                {
                    ImagePane.Source = value;
                    ImageData = Source as BitmapSource;
                    UpdateSourceInfo();
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("ImageSource"));
                }
            }
        }

        private BitmapSource ImageData;

        public event PropertyChangedEventHandler PropertyChanged;

        private void UpdateSourceInfo()
        {
            FileInfo FI = new FileInfo(ImageLabel.Content.ToString()); // TODO: Rework this
            name = FI.Name;
            path = FI.FullName;
        }

        private string name = "";
        private string path = "";
        public string ImageName
        {
            get { return name; }
            set { name = value; }
        }

        public string ImagePath
        {
            get { return path; }
            set { path = value; }
        }

        public int PixelHeight {
            get { return ImageData.PixelHeight; }
        }

        public int PixelWidth {
            get { return ImageData.PixelWidth; }
        }

        public byte[] GetPixelData() {
            int bitsPerPixel = ImageData.Format.BitsPerPixel;
            int bytesPerPixel = (bitsPerPixel + 7) / 8;
            int stride = PixelWidth * bytesPerPixel;
            byte[] pixels = new byte[PixelHeight * stride];
            ImageData.CopyPixels(pixels, stride, 0);
            return pixels;
        }
        
        private void ConvertFormat()
        {
            if (ImageData.Format != PixelFormats.Rgba128Float)
                Source = new FormatConvertedBitmap(ImageData, PixelFormats.Rgba128Float, null, 0);
        }

        
        public void SetImageFromFile(string filepath)
        {
            ImageLabel.Content = filepath;
            var converted = new ImageSourceConverter().ConvertFromString(filepath);
            var convertedSource = converted as ImageSource;
            Source = convertedSource;

            ConvertFormat();
        }

        private void ImageGrid_Drop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                string[] files = (string[])e.Data.GetData(DataFormats.FileDrop); // Note that you can have more than one file.
                SetImageFromFile(files[0]);
            }
            else if (e.Data.GetDataPresent(typeof(Image)))
            {
                Image image = e.Data.GetData(typeof(Image)) as Image;
                if (e.Data.GetDataPresent("Label"))
                    ImageLabel.Content = e.Data.GetData("Label") as string;
                Source = image.Source;
            }
            else if (e.Data.GetDataPresent(typeof(ImageSource)))
            {
                Source = e.Data.GetData(typeof(ImageSource)) as ImageSource;
            }

            ConvertFormat();
        }

        private void ImagePane_Drag(object sender, MouseButtonEventArgs e)
        {
            Image image = e.Source as Image;
            DataObject data = new DataObject(typeof(ImageSource), Source);
            data.SetData("Label", ImageLabel.Content);
            DragDrop.DoDragDrop(image, data, DragDropEffects.All);
        }

        private void ImagePane_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if(e.ClickCount == 2)
            {
                OpenCvSharp.Window window = new OpenCvSharp.Window(Source.ToString());

                Mat img = new Mat(PixelHeight, PixelWidth, MatType.CV_32FC4, GetPixelData());

                Cv2.CvtColor(img, img, ColorConversionCodes.RGBA2BGRA);

                window.ShowImage(img);
            }
            else
            {
                Image image = e.Source as Image;
                DataObject data = new DataObject(typeof(Image), image);
                data.SetData("Label", ImageLabel.Content);
                DragDrop.DoDragDrop(image, data, DragDropEffects.All);
            }
        }

        bool isThumbnail = false;
        private void ButtonToggleThumbnail_Click(object sender, RoutedEventArgs e)
        {
            if (isThumbnail)
            {
                //ImageGrid.RowDefinitions[1].Height = new GridLength(ImageData.PixelHeight);
                ImageGrid.Height = 500;
                ImageGrid.Width = double.NaN;
            }
            else
            {
                //ImageGrid.RowDefinitions[1].Height = GridLength.Auto;
                ImageGrid.Height = 200;
                ImageGrid.Width = double.NaN;
            }
            isThumbnail = !isThumbnail;
        }
    }
}
