﻿using System;
using System.Collections.Generic;

namespace CPP.TTM_Module
{
    public class TTMEnvironment
    {
        public enum APIConfigurationType
        {
            Unknown = 0,
            AJA,
            UE4,
            Resources,
            TextureQueue
        };

        public class Channel
        {
            public int spigot { get; set; }
            public string direction { get; set; }
            public string path { get; set; }
            public string videoFormat { get; set; }
            public string pixelFormat { get; set; }
        }

        public class APIConfiguration
        {
            public string name { get; set; }
            public string type { get; set; }
            public int channelCount { get; set; }
            public List<Channel> channels { get; set; }

            public APIConfigurationType GetAPIConfigurationType()
            {
                return (TTMEnvironment.APIConfigurationType)Enum.Parse(typeof(TTMEnvironment.APIConfigurationType), type);
            }
        }

        public List<APIConfiguration> apiConfigurations { get; set; }

        public APIConfiguration GetAPIConfiguration(string apiConfigName)
        {
            foreach (var apiConfig in apiConfigurations)
            {
                if (apiConfig.name == apiConfigName)
                    return apiConfig;
            }

            return null;
        }
    }


    //using System;
    //using System.Collections.Generic;
    //using System.Linq;
    //using System.Text;
    //using System.Threading.Tasks;

    //namespace CPP
    //{
//    public class Environment
//    {
//        public enum APIConfigurationType { AJA = 0, UE4 = 1, Resource = 2 };

//        public class Channel
//        {
//            public int spigot { get; set; }
//            public string direction { get; set; }
//            public string path { get; set; }
//        }

//        public class APIConfiguration
//        {
//            public string name { get; set; }
//            public APIConfigurationType type { get; set; }
//            public int channelCount { get; set; }
//            public List<Channel> channels { get; set; }
//        }

//        public List<APIConfiguration> apiConfigurations { get; set; }

//        public APIConfiguration GetAPIConfiguration(string apiConfigName)
//        {
//            foreach (var apiConfig in apiConfigurations)
//            {
//                if (apiConfig.name == apiConfigName)
//                    return apiConfig;
//            }

//            return null;
//        }
//    }
}
