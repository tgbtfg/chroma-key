﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.Windows.Interop;
using System.Windows.Media;
using System.Text;
using CCP.TTM_Module;
using System.Linq;
using CCP.Backend_Interface;

namespace TTMHostApp.TTMProcessHandling
{
    /// <summary>
    /// AppControl.xaml 
    /// </summary>
    public partial class ProcessContainerControl : UserControl, IDisposable
    {
        [System.Runtime.InteropServices.StructLayoutAttribute(System.Runtime.InteropServices.LayoutKind.Sequential)]
        public struct HWND__
        {
            /// int
            public int unused;
        }

        //public event EventHandler ProcessStarted;

        private readonly TTMComputePipeline computePipeline;

        public ProcessContainerControl(TTMComputePipeline computePipeline)
        {
            InitializeComponent();
            this.computePipeline = computePipeline;
            this.SizeChanged += new SizeChangedEventHandler(OnSizeChanged);
            this.Loaded += new RoutedEventHandler(OnVisibleChanged);
            this.SizeChanged += new SizeChangedEventHandler(OnResize);

            // populate the zoom options
            /* // Not neccessary when not using zoom and sample button options
            ZoomLevelContextMenu.Items.Clear();
            int[] zoomLevels = { 2, 3, 4, 6, 8, 10, 15, 20, 25, 50, 75, 100 };
            for (int i = 0; i < zoomLevels.Length; i++)
            {
                MenuItem menuItem = new MenuItem();
                menuItem.Header = (zoomLevels[i] * 1) + "x Zoom";
                menuItem.Click += ZoomLevelContextMenuItem_Click;
                ZoomLevelContextMenu.Items.Add(menuItem);
            }

            // populate the sampling interpolation options
            SamplingInterpolationContextMenu.Items.Clear();
            string[] samplingTypes = { "Linear", "Point" };
            for (int i = 0; i < samplingTypes.Length; i++)
            {
                MenuItem menuItem = new MenuItem();
                menuItem.Header = ToFirstCharUpperCase(samplingTypes[i]) + " Sampling";
                menuItem.Click += SamplingInterpolationContextMenuItem_Click;
                SamplingInterpolationContextMenu.Items.Add(menuItem);
            }
            */
        }

        private void ZoomLevelContextMenuItem_Click(object sender, RoutedEventArgs e)
        {
            string numberStr = (sender as MenuItem).Header.ToString().Split('x')[0];
            float number = float.Parse(numberStr);

            string command = "pipeline = get_pipeline()\n";
            command += "pipeline:set_preview_zoom(" + number + ", " + number + ")\n";
            command += "pipeline:broadcast_json_update()";
            (computePipeline as ICommandSender)?.Send(command);
        }

        private void SamplingInterpolationContextMenuItem_Click(object sender, RoutedEventArgs e)
        {
            string sampling = (sender as MenuItem).Header.ToString().Split(' ')[0];
            //sampling = sampling.ToLower();

            string command = "pipeline = get_pipeline()\n";
            command += "pipeline:set_preview_interpolation('" + sampling + "')\n";
            command += "pipeline:broadcast_json_update()";

            (computePipeline as ICommandSender)?.Send(command);
        }

        ~ProcessContainerControl()
        {
            this.Dispose();
        }

        /// <summary>
        /// Track if the application has been created
        /// </summary>
        private bool _iscreated = false;

        /// <summary>
        /// Track if the control is disposed
        /// </summary>
        private bool _isdisposed = false;

        private struct ProcessData
        {
            public WindowHandleUtility ContainerHandle;
            public WindowHandleUtility WindowHandleUtil;
            public Process ProcessObj;
        }
        ProcessData mChildProcess;

        private string exeName = "";

        public string ExeName
        {
            get
            {
                return exeName;
            }
            set
            {
                exeName = value;
            }
        }

        //public static ProcessContainerControl ProcessContainerControlObject { get; internal set; }
        //public static ConsoleControl ConsoleObject { get; internal set; }
        //public static GraphEditor.GraphControl GraphControl { get; internal set; }
        //public static EnvironmentEditor.EnvironmentControl EnvironmentControl { get; internal set; }
        //public static AssetsEditor.AssetsControl AssetsControl { get; internal set; }

        /// <summary>
        /// Force redraw of control when size changes
        /// </summary>
        /// <param name="e">Not used</param>
        protected void OnSizeChanged(object s, SizeChangedEventArgs e)
        {
            this.InvalidateVisual();
        }

        private static string ToFirstCharUpperCase(string str)
        {
            str = str.ToLower();
            StringBuilder sb = new StringBuilder(str);
            sb[0] = char.ToUpper(sb[0]);
            return sb.ToString();
        }

        /// <summary>
        /// WndProc matches the HwndSourceHook delegate signature so it can be passed to AddHook() as a callback. This is the same as overriding a Windows.Form's WncProc method.
        /// </summary>
        /// <param name="hwnd">The window handle</param>
        /// <param name="msg">The message ID</param>
        /// <param name="wParam">The message's wParam value, historically used in the win32 api for handles and integers</param>
        /// <param name="lParam">The message's lParam value, historically used in the win32 api to pass pointers</param>
        /// <param name="handled">A value that indicates whether the message was handled</param>
        /// <returns></returns>
        private /*static*/ IntPtr WndProc(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
        {
            var message = (WindowMessage)msg;
            var subCode = (WindowMessageParameter)wParam.ToInt32();

            if (message == WindowMessage.WM_COPYDATA)
            {
                COPYDATASTRUCT_IN data = (COPYDATASTRUCT_IN)Marshal.PtrToStructure(lParam, typeof(COPYDATASTRUCT_IN));
                string str = System.Runtime.InteropServices.Marshal.PtrToStringAnsi(data.lpData);
                UInt32 upperMask = (UInt32)0xFFFF0000;
                UInt32 downMask = (UInt32)0x0000FFFF;
                UInt32 intention = (data.dwData & upperMask) >> 16;
                UInt32 code = data.dwData & downMask;

                switch (intention)
                {
                    case 0: // message
                        {
                            TextBlock myTextBlock = new TextBlock
                            {
                                TextWrapping = TextWrapping.Wrap,
                                Margin = new Thickness(0, 0, 0, 0),
                                Text = str
                            };

                            if (code == 1) // warning
                                myTextBlock.Foreground = new SolidColorBrush(Colors.DarkOrange);
                            if (code == 0) // error
                                myTextBlock.Foreground = new SolidColorBrush(Colors.Red);

                            //ConsoleObject.OutputLogger.Children.Add(myTextBlock);
                        }
                        break;

                    case 1: // instruction
                        {
                            switch (code)
                            {
                                case 0: // PARENT_INSTRUCTION_CODE_SET_CONFIGURATION
                                    {
                                        computePipeline.UpdatePipelineFromJSON(str);
                                        computePipeline.UpdateEnvironmentFromJSON(str);
                                        //EnvironmentControl.UpdatePipelineFromJSON(str);
                                    }
                                    break;

                                case 1: // PARENT_INSTRUCTION_CODE_SET_PIPELINE
                                    {
                                        computePipeline.UpdatePipelineFromJSON(str);
                                        //GraphControl.NodeGraphViewModel.ConstructFromJSON(str);
                                        //ProcessContainerControlObject.nodeNameTxtBox.Text = GraphControl.NodeGraphViewModel.Pipeline.previewNode;
                                        //ProcessContainerControlObject.ZoomLvlBtn.Content = GraphControl.NodeGraphViewModel.Pipeline.zoomIntensityX + "x Zoom";
                                        //ProcessContainerControlObject.SamplingInterpolationBtn.Content = ToFirstCharUpperCase(GraphControl.NodeGraphViewModel.Pipeline.previewSamplingInterpolation) + " Sampling";
                                    }
                                    break;

                                case 2: // PARENT_INSTRUCTION_CODE_PROCESS_DETAILS
                                    {
                                        // not needed when used in a way implemented here
                                    }
                                    break;

                                case 3: // PARENT_INSTRUCTION_CODE_SEND_FILE_NAME
                                    {
                                        // not needed when used in a way implemented here
                                    }
                                    break;

                                case 4: // PARENT_INSTRUCTION_CODE_UPDATE_PIPELINE
                                    {
                                        computePipeline.UpdatePipelineFromJSON(str);
                                        //GraphControl.NodeGraphViewModel.UpdatePipelineFromJSON(str);
                                        // ProcessContainerControlObject.nodeNameTxtBox.Text = GraphControl.NodeGraphViewModel.Pipeline.previewNode;
                                        // ProcessContainerControlObject.ZoomLvlBtn.Content = GraphControl.NodeGraphViewModel.Pipeline.zoomIntensityX + "x Zoom";
                                        // ProcessContainerControlObject.SamplingInterpolationBtn.Content = ToFirstCharUpperCase(GraphControl.NodeGraphViewModel.Pipeline.previewSamplingInterpolation) + " Sampling";
                                    }
                                    break;

                                case 5: // PARENT_INSTRUCTION_CODE_UPDATE_FPS
                                    {
                                        //ProcessContainerControlObject.fpsTxtBox.Text = str;
                                    }
                                    break;

                                case 6: // PARENT_INSTRUCTION_CODE_SET_ASSETS
                                    {
                                        computePipeline.UpdatePipelineFromJSON(str);
                                        // AssetsControl.UpdatePipelineFromJSON(str);
                                    }
                                    break;

                                default:
                                    break;
                            }
                        }
                        break;

                    default:
                        break;
                }
            }

            return IntPtr.Zero;
        }

        /// <summary>
        /// Send request to TTM main app.
        /// </summary>
        /*
        void ICommandSender.Send(string command)
        {
            COPYDATASTRUCT_OUT cds;
            cds.dwData = 100;
            cds.lpData = Marshal.StringToHGlobalAnsi(command);
            cds.cbData = command.Length + 1;

            IntPtr cdBuffer = WindowHandleUtility.IntPtrAlloc(cds);
            mChildProcess.WindowHandleUtil.SendMessage((int)WindowMessage.WM_COPYDATA, IntPtr.Zero, cdBuffer);
            WindowHandleUtility.IntPtrFree(cdBuffer);
        }
        */

        /// <summary>
        /// Create control when visibility changes
        /// </summary>
        /// <param name="e">Not used</param>
        protected void OnVisibleChanged(object s, RoutedEventArgs e)
        {
            // If control needs to be initialized/created
            if (_iscreated == false)
            {
                // Mark that control is created
                _iscreated = true;

                // Initialize handle value to invalid
                mChildProcess.WindowHandleUtil = null;

                try
                {
                    // Get the current process.
                    Process currentProcess = Process.GetCurrentProcess();
                    // Get my handle
                    var hwndHelper = new WindowInteropHelper(Window.GetWindow(this.ProcessContainer));
                    mChildProcess.ContainerHandle = new WindowHandleUtility(hwndHelper.Handle);
                    var hwndSource = HwndSource.FromHwnd(hwndHelper.Handle);
                    if (hwndSource != null) hwndSource.AddHook(WndProc);

                    // Get the position
                    //Point relativePoint = this.ProcessContainer.TransformToAncestor(Application.Current.MainWindow).Transform(new Point(0, 0));
                    Point relativePoint = this.ProcessContainer.TransformToAncestor(Window.GetWindow(this.ProcessContainer)).Transform(new Point(0, 0));

                    // Start the process
                    mChildProcess.ProcessObj = System.Diagnostics.Process.Start(this.exeName,
                        "parentProcID:" + currentProcess.Id.ToString() + ";" +
                        "parentProcHandle:" + currentProcess.Handle.ToString() + ";" +
                        "parentWinHwnd:" + hwndHelper.Handle.ToString() + ";" +
                        "X:" + relativePoint.X.ToString() + ";" +
                        "Y:" + relativePoint.Y.ToString() + ";" +
                        "W:" + this.ProcessContainer.ActualWidth.ToString() + ";" +
                        "H:" + this.ProcessContainer.ActualHeight.ToString() + ";");

                    // Wait for process to be created and enter idle condition
                    mChildProcess.ProcessObj.WaitForInputIdle();

                    // Get the main handle
                    var allChildWindows = mChildProcess.ContainerHandle.GetAllChildHandles();
                    mChildProcess.WindowHandleUtil = new WindowHandleUtility(allChildWindows[0]);

                    // dispatch the event
                    //ProcessStarted.Invoke(this, EventArgs.Empty);
                }
                catch (Exception ex)
                {
                    mChildProcess.WindowHandleUtil = null;
                    Debug.Print(ex.Message + "Error");
                }
            }
        }

        /// <summary>
        /// Update display of the executable
        /// </summary>
        /// <param name="e">Not used</param>
        protected void OnResize(object s, SizeChangedEventArgs e)
        {
            if (this.mChildProcess.WindowHandleUtil != null)
            {
                //Point relativePoint = this.ProcessContainer.TransformToAncestor(Application.Current.MainWindow).Transform(new Point(0, 0));
                Point relativePoint = this.ProcessContainer.TransformToAncestor(Window.GetWindow(this.ProcessContainer)).Transform(new Point(0, 0));
                mChildProcess.WindowHandleUtil.MoveWindow((int)relativePoint.X, (int)relativePoint.Y, (int)this.ProcessContainer.ActualWidth, (int)this.ProcessContainer.ActualHeight);
            }
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_isdisposed)
            {
                if (disposing)
                {
                    if (_iscreated && mChildProcess.WindowHandleUtil != null && !mChildProcess.ProcessObj.HasExited)
                    {
                        // Stop the application
                        //mChildProcess.ProcessObj.Kill();
                        (computePipeline as ICommandSender)?.Send("exit()");

                        // Waits here for the process to exit.
                        mChildProcess.ProcessObj.WaitForExit();

                        // Clear internal handle
                        mChildProcess.WindowHandleUtil = null;
                    }
                }
                _isdisposed = true;
            }
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void CaptureBtn_Click(object sender, RoutedEventArgs e)
        {
            // Prepare the cache
            /*
            GraphEditor.Pipeline.Node previewNode = GraphControl.NodeGraphViewModel.GetPreviewedNode();
            string cacheName = "previewCache";
            if ((previewNode as object) != null)
            {
                string command = "pipeline = get_pipeline()\n";
                command += "node = pipeline:get_node('" + previewNode.name + "')\n";
                command += "result = node:capture_resource_to_cache('" + cacheName + "')";
                (pipeline as ICommandSender)?.Send(command);
            }

            // Display the dialogue
            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            dlg.FileName = "Untitled"; // Default file name
            dlg.DefaultExt = ".png"; // Default file extension
            dlg.Filter = "PNG file (*.png)|*.png"; // Filter files by extension

            // Show save file dialog box
            bool? result = dlg.ShowDialog();
            if (result == true)
            {
                // Save document
                string filename = dlg.FileName;
                filename = filename.Replace("\\", "/");
                string command = "save_cache_to_file('" + cacheName + "', '" + filename + "')";
                (pipeline as ICommandSender)?.Send(command);
            }
            */
        }

        private void ZoomLvlBtn_Click(object sender, RoutedEventArgs e)
        {
            (sender as Button).ContextMenu.IsEnabled = true;
            (sender as Button).ContextMenu.PlacementTarget = (sender as Button);
            (sender as Button).ContextMenu.Placement = System.Windows.Controls.Primitives.PlacementMode.Bottom;
            (sender as Button).ContextMenu.IsOpen = true;
        }

        private void SamplingInterpolationBtn_Click(object sender, RoutedEventArgs e)
        {
            (sender as Button).ContextMenu.IsEnabled = true;
            (sender as Button).ContextMenu.PlacementTarget = (sender as Button);
            (sender as Button).ContextMenu.Placement = System.Windows.Controls.Primitives.PlacementMode.Bottom;
            (sender as Button).ContextMenu.IsOpen = true;
        }
    }
}
