﻿using CCP;
using CCP.Backend_Interface;
using CPP.TTM_Module;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using TTMHostApp.TTMProcessHandling;


namespace CCP.TTM_Module
{
    public class TTMComputePipeline : IComputePipeline, ICommandSender
    {
        private IPipelineGraph PipelineModel { get; set; }
        public TTMEnvironment EnvironmentData { get; internal set; }
        public ICommandSender TTMCommandSender { get; internal set; }

        private readonly TTMSocketClientTCP client = new TTMSocketClientTCP();

        public ProcessContainerControl localTTMProcessContainer;

        public PipelineManagerControl managerControl;

        public TTMComputePipeline(PipelineManagerControl pipelineManagerControl)
        {
            managerControl = pipelineManagerControl;
            TTMCommandSender = this;
        }

        public void UpdateEnvironmentFromJSON(String json)
        {
            EnvironmentData = JsonConvert.DeserializeObject<TTMEnvironment>(json);
        }

        public void UpdatePipelineFromJSON(String json)
        {
            TTMPipeline pipelineObject = JsonConvert.DeserializeObject<TTMPipeline>(json);
            if (pipelineObject != null && (PipelineModel == null || (PipelineModel as TTMPipeline).nodes == null))
            {
                PipelineModel = pipelineObject;
                managerControl.PopulateNodeViewFromPipeline(PipelineModel);
            }
        }

        private void SetResourceConfig()
        {
            // Make and set Resource APIConfiguration
            TTMEnvironment.APIConfiguration resourceConfig = EnvironmentData.GetAPIConfiguration("RESOURCE_IN");
            if (resourceConfig.GetAPIConfigurationType() == TTMEnvironment.APIConfigurationType.Resources)
            {
                // TODO: reconsider direct access to control page here
                foreach (NodeControl nodeControl in managerControl.PipelineControlPanel.PipelineControlStack.Children)
                {
                    if (nodeControl != null && nodeControl.PipelineNode is TTMPipeline.Node)
                    {
                        TTMPipeline.Node node = nodeControl.PipelineNode as TTMPipeline.Node;
                        if (node.type == NodeType.Input && node.configuration == "RESOURCE_IN"
                            && nodeControl.inputImageSlot.Source != null)
                        {
                            TTMEnvironment.Channel channel = resourceConfig.channels[node.channel];
                            channel.direction = "in";
                            string pathstring = nodeControl.inputImageSlot.ImageLabel.Content.ToString();
                            FileInfo info = new FileInfo(pathstring);
                            channel.path = "textures/" + info.Name; // TODO: make more robust
                        }
                        else
                        {
                            // TODO: Re-set other parameters to current values, etc. Use parameter map ? 
                        }
                    }
                }
            }
        }

        public void UpdateEnvironment()
        {
            string command = "pipeline = get_pipeline()\n";
            command += "environment = get_environment()\n";
            command += "assets = get_assets()\n";
            command += "pipeline:unload()\n";
            command += "environment:unload()\n";

            SetResourceConfig();

            string jsonStr = JsonConvert.SerializeObject(EnvironmentData, Formatting.Indented);
            jsonStr = jsonStr.Replace("\r", "\\r");
            jsonStr = jsonStr.Replace("\n", "\\n");
            jsonStr = jsonStr.Replace("\t", "\\t");
            command += "environment_json = '" + jsonStr + "'\n";
            command += "result = assets:update_file_with_string('environment.json', environment_json)\n";
            command += "environment:load(assets)\n";
            command += "pipeline:load(assets)\n";
            command += "pipeline:initialize()\n";
            command += "environment:broadcast_json_update()";
            TTMCommandSender.Send(command);
        }

        void IComputePipeline.AddAsset(string assetName, string assetPath)
        {
            string command = "assets = get_assets()\n";
            assetPath = assetPath.Replace("\\", "/");
            command += "result = assets:add_file('" + assetName + "', '" + assetPath + "')\n";
            command += "assets:broadcast_json_update()";
            TTMCommandSender.Send(command);
        }

        Type IComputePipeline.GetParameterType(string parameterName)
        {
            throw new NotImplementedException(); // TODO: Implement
        }

        object[] IComputePipeline.GetParameterValue(string parameterName)
        {
            throw new NotImplementedException(); // TODO: Implement
        }

        string IComputePipeline.GetView()
        {
            return (PipelineModel as TTMPipeline).previewNode;
        }

        void IComputePipeline.OpenProject(string projectPath)
        {
            // Todo: Reconsider using direct access to Control Page here
            //ControlPage.CurrentControlPage.PipelineControlPanel.SetTitle(projectPath);
            PipelineModel = null; // Indicate to rebuild pipeline model by deleting old one
            string command = "load_project('" + projectPath.Replace("\\", "/") + "')";
            TTMCommandSender.Send(command);
        }

        void IComputePipeline.RemoveAsset(string assetName)
        {
            throw new NotImplementedException(); // TODO: Implement
        }

        void IComputePipeline.SetInputImage(object image)
        {
            ImageSlot imageSlot = image as ImageSlot;
            (this as IComputePipeline).AddAsset("textures/" + imageSlot.ImageName, imageSlot.ImagePath);
            UpdateEnvironment();
        }

        void IComputePipeline.SetParameter(string nodeName, string parameterName, object parameterValues)
        {
            try
            {
                if(parameterValues is JToken)
                {
                    JToken token = parameterValues as JToken;

                    //dynamic parameters = JsonConvert.DeserializeObject<dynamic>(parameterValues);
                    dynamic parameters = token;
                    string parameterString = "";
                    //if (parameters is double)
                    //    parameterString = ((double)parameters).ToString("0.0000", System.Globalization.CultureInfo.InvariantCulture);
                    //else
                    //{
                    //foreach (var childtoken in parameters)
                    if (token.HasValues)
                    {
                        foreach (var childtoken in parameters)
                        {
                            double val = childtoken.Value;
                            parameterString += val.ToString("0.0000", System.Globalization.CultureInfo.InvariantCulture) + ", ";
                        }
                        int i = parameterString.LastIndexOf(',');
                        parameterString = parameterString.Remove(i, 1);
                    }
                    else
                    {
                        parameterString = parameters.Value.ToString("0.0000", System.Globalization.CultureInfo.InvariantCulture);
                    }
                    //}
                    string command = "pipeline = get_pipeline()\n";
                    command += "node = pipeline:get_node('" + nodeName + "')\n";
                    command += "node:set_param_value('" + parameterName + "'," + parameterString + ")";
                    TTMCommandSender.Send(command);
                }
            } catch(Exception e)
            {
                Console.WriteLine(e.ToString());
            }

            //PipelineManager.CurrentParameterControlRegistry.SetControlParameterValue(nodeName, parameterName, parameterValues);
        }

        void IComputePipeline.SetView(string viewName)
        {
            if (viewName != null)
            {
                (PipelineModel as TTMPipeline).previewNode = viewName;
                string command = "pipeline = get_pipeline()\n";
                command += "node = pipeline:get_node('" + viewName + "')\n";
                command += "node:set_for_preview()\n";
                command += "pipeline:broadcast_json_update()";
                TTMCommandSender.Send(command);
            }
        }

        bool IComputePipeline.IsInitialized()
        {
            return PipelineModel != null;
        }

        bool IComputePipeline.Connect(string ip, string port)
        {
            return client.Connect(ip, port);
        }

        void ICommandSender.Send(string command)
        {
            Int32 commandLength = command.Length;
            //Assert(BitConverter.IsLittleEndian);
            byte[] headerBytes = BitConverter.GetBytes(commandLength);
            byte[] byteData = Encoding.ASCII.GetBytes(command);

            byte[] message = new byte[sizeof(Int32) + byteData.Length];
            headerBytes.CopyTo(message, 0);
            byteData.CopyTo(message, headerBytes.Length);

            Int32 remaining = message.Length;

            const int buffersize = StateObject.BufferSize;
            while (remaining > 0)
            {
                Int32 packetsize = remaining < buffersize ? remaining : buffersize; // Min
                Int32 sentSize = message.Length - remaining;
                byte[] packet = message.Skip(sentSize).Take(packetsize).ToArray();
                client.Send(packet);
                string sent = Encoding.ASCII.GetString(packet, 0, packet.Length);
                if (TTMSocketClientTCP.enableConsoleOutput) Console.WriteLine("Sending command: {0}", sent);
                remaining -= packetsize;
            }
        }

        // Mapping from node and parameter name to control
        //TODO: remove //private Dictionary<string, Dictionary<string, IParameterControl>> ControlRegistry; // TODO, separate out / refactor. Should perhaps not have direct access to map of parameter controls

        // TODO(?): Rename to 'Register', 'Attach', 'Subscribe', or similar?
        /*
        void IComputePipeline.Connect(IParameterControl control, string nodeName, string parameterName)
        {
            reg.RegisterParameterControl(control, nodeName, parameterName);
        }

        ParameterMap IComputePipeline.CreateParameterMapFromControls()
        {
            return reg.CreateParameterMapFromControls();
        }
        */


        public string CaptureCurrentFrame()
        {
            var previewNode = (this as IComputePipeline).GetView();
            string cacheName = "previewCache";
            if ((previewNode as object) != null)
            {
                string command = "pipeline = get_pipeline()\n";
                command += "node = pipeline:get_node('" + previewNode + "')\n";
                command += "result = node:capture_resource_to_cache('" + cacheName + "')";
                TTMCommandSender?.Send(command);
            }

            // Display the dialogue
            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog
            {
                FileName = "Untitled", // Default file name
                DefaultExt = ".png", // Default file extension
                Filter = "PNG file (*.png)|*.png" // Filter files by extension
            };

            // Show save file dialog box
            bool? result = dlg.ShowDialog();
            if (result == true)
            {
                // Save document
                string filename = dlg.FileName;
                filename = filename.Replace("\\", "/");
                string command = "save_cache_to_file('" + cacheName + "', '" + filename + "')";
                TTMCommandSender?.Send(command);


                return filename;
                //managerControl.AddImageSlot(filename);
            }
            return "";
        }
    }
}
