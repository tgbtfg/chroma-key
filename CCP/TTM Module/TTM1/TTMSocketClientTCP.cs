﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CCP.TTM_Module
{
    // State object for receiving data from remote device.  



    class TTMSocketClientTCP
    {
        // The port number for the remote device.  
        public const int port = 27015;

        // ManualResetEvent instances signal completion.  
        private ManualResetEvent connectDone = new ManualResetEvent(false);
        private ManualResetEvent sendDone    = new ManualResetEvent(false);
        private ManualResetEvent receiveDone = new ManualResetEvent(false);

        private Socket clientSocket;

        // The response from the remote device.  
        private String response = String.Empty;

        public static bool enableConsoleOutput = true;

        public TTMSocketClientTCP()
        {
            //currentConnection = this;
        }

        public bool Connect(string ip, string port)
        {
            try // Connect to a remote device.  
            {
                IPAddress ipAddress = IPAddress.Parse(ip);  // Establish the remote endpoint for the socket.
                int portnumber = Int32.Parse(port);
                IPEndPoint remoteEP = new IPEndPoint(ipAddress, portnumber);
                clientSocket = new Socket(ipAddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp); // Create a TCP/IP socket.  
                clientSocket.BeginConnect(remoteEP, new AsyncCallback(ConnectCallback), clientSocket); // Connect to the remote endpoint.  
                connectDone.WaitOne();
                bool hasReceived = Receive(clientSocket);
                return hasReceived;
            }
            catch (Exception e) {
                PipelineManager.ShowMessage("Failed to connect...");
                Console.WriteLine(e.ToString()); }
            return false;
        }

        private void ConnectCallback(IAsyncResult ar)
        {
            try {
                Socket client = (Socket)ar.AsyncState; // Retrieve the socket from the state object.  
                client.EndConnect(ar); // Complete the connection.  
                if (enableConsoleOutput) Console.WriteLine("Socket connected to {0}", client.RemoteEndPoint.ToString());
                PipelineManager.ShowMessage(String.Format("Socket connected to {0}", client.RemoteEndPoint.ToString()));
            }
            catch (Exception e) {
                PipelineManager.ShowMessage("Failed to connect.");
                Console.WriteLine(e.ToString()); }
            connectDone.Set(); // Signal that the connection has been attempted.  
        }

        private bool Receive(Socket client)
        {
            try {
                StateObject state = new StateObject { workSocket = client }; // Create the state object.  
                client.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0, new AsyncCallback(ReceiveCallback), state); // Begin receiving the data from the remote device.  
                return true;
            }
            catch (Exception e) {
                PipelineManager.ShowMessage("Failed to receive TCP message from pipeline.");
                Console.WriteLine(e.ToString());
            }
            return false;
        }

        private void ReceiveCallback(IAsyncResult ar)
        {
            try
            {
                // Retrieve the state object and the client socket   
                // from the asynchronous state object.  
                StateObject state = (StateObject)ar.AsyncState;
                Socket client = state.workSocket;
                int bytesRead = client.EndReceive(ar); // Read data from the remote device.  

                if (bytesRead > 0)
                {
                    // There might be more data, so store the data received so far.  
                    string received = Encoding.ASCII.GetString(state.buffer, 0, bytesRead);
                    if (enableConsoleOutput) Console.WriteLine("Response received : {0}", received);
                    state.sb.Append(received);

                    // Get the rest of the data.  
                    client.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0, new AsyncCallback(ReceiveCallback), state);
                }
                else
                {
                    // All the data has arrived; put it in response.  
                    if (state.sb.Length > 1)
                    {
                        response = state.sb.ToString();
                    }
                    receiveDone.Set(); // Signal that all bytes have been received.  
                    if (enableConsoleOutput) Console.WriteLine("Response received : {0}", response);
                }
            }
            catch (Exception e) { Console.WriteLine(e.ToString()); }
        }

        public void Send(Socket client, String data)
        {
            byte[] byteData = Encoding.ASCII.GetBytes(data); // Convert the string data to byte data using ASCII encoding.  
            client.BeginSend(byteData, 0, byteData.Length, 0, new AsyncCallback(SendCallback), client); // Begin sending the data to the remote device.  
        }

        public void Send(byte[] byteData)
        {
            if (clientSocket?.Connected != null)
                try
                {
                    clientSocket?.BeginSend(byteData, 0, byteData.Length, 0, new AsyncCallback(SendCallback), clientSocket);
                } catch(Exception e)
                {
                    PipelineManager.ShowMessage("Failed to send.");
                    Console.WriteLine(e.ToString());
                }
        }

        private void SendCallback(IAsyncResult ar)
        {
            try {
                Socket client = (Socket)ar.AsyncState; // Retrieve the socket from the state object.
                int bytesSent = client.EndSend(ar); // Complete sending the data to the remote device.  
                if(enableConsoleOutput) Console.WriteLine("Sent {0} bytes to server.", bytesSent);
                sendDone.Set(); // Signal that all bytes have been sent.  
            }
            catch (Exception e) {
                PipelineManager.ShowMessage("Failed to send.");
                Console.WriteLine(e.ToString());
            }
        }
    }
}
