﻿using CCP.Backend_Interface;
using System.Collections.Generic;

namespace CPP.TTM_Module
{
    public class TTMPipeline : IPipelineGraph
    {
        public class Parameter : IParameter
        {
            public string name { get; set; }
            public string type { get; set; }
            public object[] initialValue { get; set; }
        }

        //public enum NodeType { Input = 0, ComputeShader = 1, Output = 2 };

        public class Node : IPipelineNode
        {
            public string name { get; set; }
            public NodeType type { get; set; }
            public int width { get; set; }
            public int height { get; set; }
            public string format { get; set; }
            public int threadGroupSizeX { get; set; }
            public int threadGroupSizeY { get; set; }
            public int threadGroupSizeZ { get; set; }
            public string shaderCode { get; set; }
            public List<string> shaderIncludes { get; set; }
            public string configuration { get; set; }
            public int channel { get; set; }
            public List<Parameter> parameters { get; set; }

            public int posX { get; set; }
            public int posY { get; set; }

            public static bool operator ==(Node left, Node right)
            {
                return (left.name == right.name);
            }

            public static bool operator !=(Node left, Node right)
            {
                return (left.name != right.name);
            }

            public override bool Equals(object obj)
            {
                return this.name == ((Node)obj).name;
            }
            public override int GetHashCode()
            {
                return name.GetHashCode();
            }

            public Parameter GetParameter(string name)
            {
                foreach (var param in parameters)
                {
                    if (param.name == name) return param;
                }
                return null;
            }

            List<IParameter> IPipelineNode.GetParameters()
            {
                List<IParameter> ParameterList = parameters?.ConvertAll(x => (IParameter)x);
                return ParameterList;
            }

            public bool isImageNode()
            {
                return (type == NodeType.Input) && (configuration == "RESOURCE_IN"); // RESOURCE_IN is used for image input nodes
            }
        }

        public string previewNode { get; set; }
        public string previewSamplingInterpolation { get; set; }
        public float zoomIntensityX { get; set; }
        public float zoomIntensityY { get; set; }
        public List<Node> nodes { get; set; }
        public List<List<string>> pipes { get; set; }

        List<IPipelineNode> IPipelineGraph.GetNodes()
        {
            List<IPipelineNode> NodeList = nodes?.ConvertAll(x => (IPipelineNode)x);
            // Alternate method // List<IPipelineNode> NodeList = nodes.Cast<IPipelineNode>().ToList();
            return NodeList;
        }
    }
}
