﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace CCP.TTM_Module
{
    public class StateObject
    {
        public Socket workSocket = null; // Client socket.
        public const int BufferSize = 512; // Size of receive buffer.
        public byte[] buffer = new byte[BufferSize]; // Receive buffer.  
        public StringBuilder sb = new StringBuilder(); // Received data string.
    }
}
