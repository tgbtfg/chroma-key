﻿using CCP.Backend_Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CCP.TTM_Module
{
    public class TTM2Pipeline : IPipelineGraph
    {
        public class Parameter : IParameter
        {
            string IParameter.name { get; set; }
            string IParameter.type { get; set; }
        }

        public class Node : IPipelineNode
        {
            public string name { get; set; }
            public NodeType type { get; set; }

            List<IParameter> IPipelineNode.GetParameters()
            {
                return new List<IParameter>();
            }

            bool IPipelineNode.isImageNode()
            {
                return false;
            }
        }
        List<IPipelineNode> IPipelineGraph.GetNodes()
        {
            return new List<IPipelineNode>();
        }
    }
}
