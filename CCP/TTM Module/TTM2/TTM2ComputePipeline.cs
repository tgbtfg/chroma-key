﻿using CCP;
using CCP.Backend_Interface;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CCP.TTM_Module
{
    public class TTM2ComputePipeline : IComputePipeline, ICommandReceiver, ICommandSender
    {
        //private ICommandSender TTMCommandSender { get; set; }

        private readonly TTM2SocketClientTCP client;

        // private ParameterControlRegistry reg = new ParameterControlRegistry();
        // TODO: Add parameter map
        // private static ParameterMap Parameters { get;  set; } 
        /*
        public TTM2ComputePipeline(ICommandSender commandSender)
        {
            TTMCommandSender = commandSender;
            CurrentComputePipeline = this;
            client = new TTM2SocketClientTCP();
            //ControlRegistry = new Dictionary<string, Dictionary<string, IParameterControl>>();
        }
        */

        public TTM2ComputePipeline()
        {
            client = new TTM2SocketClientTCP();
        }

        void IComputePipeline.AddAsset(string assetName, string assetPath)
        {
            throw new NotImplementedException(); // TODO: Implement
        }

        Type IComputePipeline.GetParameterType(string parameterName)
        {
            throw new NotImplementedException(); // TODO: Implement
        }

        object[] IComputePipeline.GetParameterValue(string parameterName)
        {
            throw new NotImplementedException(); // TODO: Implement
        }

        string IComputePipeline.GetView()
        {
            throw new NotImplementedException(); // TODO: Implement
        }

        void IComputePipeline.OpenProject(string projectPath)
        {
            throw new NotImplementedException(); // TODO: Implement
        }

        void IComputePipeline.RemoveAsset(string assetName)
        {
            throw new NotImplementedException(); // TODO: Implement
        }

        void IComputePipeline.SetInputImage(object image)
        {
            throw new NotImplementedException(); // TODO: Implement
        }

        void IComputePipeline.SetParameter(string nodeName, string parameterName, object parameterValues)
        {
            string parameterString = JsonConvert.SerializeObject(parameterValues);
            string  command = "{ \"" + parameterName + "\" : " + parameterString + " }";
            (this as ICommandSender).Send(command);

            //PipelineManager.CurrentParameterControlRegistry.SetControlParameterValue(nodeName, parameterName, parameterValues);
        }

        void IComputePipeline.SetView(string viewName)
        {
            throw new NotImplementedException(); // TODO: Implement
        }

        bool IComputePipeline.IsInitialized()
        {
            return true; // throw new NotImplementedException(); // TODO: Implement
        }

        void ICommandSender.Send(string command)
        {
            Int32 commandLength = command.Length;
            byte[] byteData = Encoding.ASCII.GetBytes(command);

            byte[] message = new byte[byteData.Length + 1]; // assuming zero initialized
            byteData.CopyTo(message, 0);

            Int32 remaining = message.Length;

            const int buffersize = StateObject.BufferSize;
            while (remaining > 0)
            {
                Int32 packetsize = remaining < buffersize ? remaining : buffersize; // Min
                Int32 sentSize = message.Length - remaining;
                byte[] packet = message.Skip(sentSize).Take(packetsize).ToArray();
                client.Send(packet);
                string sent = Encoding.ASCII.GetString(packet, 0, packet.Length);
                if (TTM2SocketClientTCP.enableConsoleOutput) Console.WriteLine("Sending command: {0}", sent);
                remaining -= packetsize;
            }
        }

        void ICommandReceiver.Receive(string command)
        {
            throw new NotImplementedException();
        }

        bool IComputePipeline.Connect(string ip, string port)
        {
            return client.Connect(ip, port);
        }

        // Mapping from node and parameter name to control
        //private Dictionary<string, Dictionary<string, IParameterControl>> ControlRegistry; // TODO, separate out / refactor. Should perhaps not have direct access to map of parameter controls

        // TODO(?): Rename to 'Register', 'Attach', 'Subscribe', or similar?
        // TODO: Factor out of here, not TTM specific stuff left.

        /*
    void IComputePipeline.Connect(IParameterControl control, string nodeName, string parameterName)
    {
        reg.RegisterParameterControl(control, nodeName, parameterName);
    }

    ParameterMap IComputePipeline.CreateParameterMapFromControls()
    {
        return reg.CreateParameterMapFromControls();
    }
    */
    }
}
