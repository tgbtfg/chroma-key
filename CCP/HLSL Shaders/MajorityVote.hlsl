﻿/*
    Example Majority Vote alpha adjustment function.
    Inspired by (but not equal to): https://en.wikipedia.org/wiki/Majority_function

    Increasin the strength of this filter tends to improve alphas of background and foreground regions, at the expense of
    accurate edge and transition regions.

    Parameters:
    voteUp    : Scales degree to which neighbors' vote increase the alpha value. Default value: 1.
    voteDown  : Scales degree to which neighbors' vote decrease the alpha value. Default value: 1.
    voteGain  : Adjusts the falloff curve of the alpha adjustment. Default value: 2.
    voteScale : Scales the effect of the alpha adjustment. Default value: 0. (no effect), Range: 0 to 1.

*/

float votingFunction(float self, float others)
{
    float bias = others - self;
	
    // These adjustment functions are only examples, other conjunction- and disjunction- like operators can be used
    float votePositive = self * others; // self AND others agree to vote yes
    float voteNegative = (1. - self) * (1. - others); // self AND others agree to vote no
	
    float2 voteAdjust = float2(votePositive, voteNegative);
    voteAdjust = pow(voteAdjust, voteGain);
	
    float2 voteBalance = float2(voteUp, -voteDown);
    float finalVote = dot(voteAdjust, voteBalance) * voteScale;
	
    return self + finalVote;
}


void main(int groupIndex, int3 dispatchThreadID, int3 groupID, int3 groupThreadID)
{
    int2 p = dispatchThreadID.xy;

    float Accum  = 0.; // Accumulated alpha samples from neighborhood

    // Accumulated total neighborhood weights ( Not used here, but could be adjusted to favor spatially close neighbors)
    // It could also be modified to take similar colors from input video into account, ala bilateral filtering,
    // in which case this is not strictly speaking a pure alpha adjustment step anymore.
    float Weight = 0.; 

    int r = 3; // TODO: the neighborhood radius could be parameterized
    float W = 0.; // Total weight ( not used here )
    for (int i = -r; i <= r; i++)
    {
        for (int j = -r; j <= r; j++)
        {
            float w = 1.; // Not really used here
            float Sample = w * KeyerAlpha[p + int2(i, j)].r; // Alpha estimate neighborhood sample
            Accum += Sample;
            W += w;
        }
    }

    Accum /= W; // Accum / float((2 * r + 1) * (2 * r + 1));

    float self = KeyerAlpha[p].r;

    self  = clamp(self, 0., 1.);
    Accum = clamp(Accum, 0., 1.);

    float result = votingFunction(self, Accum);
     
    gOutput[p] = result;
}