﻿float3 segmentProject(float3 P, float3 Q, float3 X)
{
    float3 PQ = Q - P;
    float3 PX = X - P;
	
    float lineCoord = dot(PX, PQ) / dot(PQ, PQ);
	
    float segmentCoord = clamp(lineCoord, 0., 1.);
	
    return segmentCoord * PQ + P;
}

float3 capsuleDistance(float3 P, float3 Q, float r1, float r2, float3 X)
{
    float3 Y = segmentProject(P, Q, X);
    return distance(X, Y);
}

float3 scaleTransform(float3 Axis, float scaleFactor, float3 v)
{
    float3 N = normalize(Axis);
    float coord = dot(v, N);
    float3 proj = N * coord;
    float3 diff = v - proj;
    float3 scaled = N * coord * scaleFactor + diff;
    return scaled;
}

void main(int groupIndex, int3 dispatchThreadID, int3 groupID, int3 groupThreadID)
{
    int2 p = dispatchThreadID.xy;
	
    Texture2D<float4> VideoFeed = AJAFeed;
	
	// Make constant buffer parameter
	//float LumaScaling = .5;   // Scale background luma _sensitivity_
	//float PrimaryScaling = 2.; // Scale background primary chromaticity _sensitivity_
	//float SecondaryScaling = 2.; // Scale background secondary chromaticity _sensitivity_
	
    float3 C = VideoFeed[p].rgb;
	//float3 B = BackgroundColor[int2(0, 0)].rgb; // Not used anymore

	// Background : High, Mid and Low luma parameters
    float3 B_Hi = BackgroundHighlight.rgb; // (Constant buffer parameter)
    float3 B_Mi = BackgroundMidtones.rgb; // (Constant buffer parameter)
    float3 B_Lo = BackgroundShadows.rgb; // (Constant buffer parameter)

    float3 LumaAxis = 1.;
    float3 PrimaryAxis = B_Mi - dot(B_Mi, 1.) / 3.; //dot(B_Mi, LumaAxis)/dot(LumaAxis, LumaAxis)
    float3 SecondaryAxis = normalize(cross(LumaAxis, PrimaryAxis));

	// Nonuniform colorspace scaling (Luma + Primary and Secondary background chroma axes)  
    C = scaleTransform(PrimaryAxis, PrimaryScaling, C);
    B_Hi = scaleTransform(PrimaryAxis, PrimaryScaling, B_Hi);
    B_Mi = scaleTransform(PrimaryAxis, PrimaryScaling, B_Mi);
    B_Lo = scaleTransform(PrimaryAxis, PrimaryScaling, B_Lo);

    C = scaleTransform(LumaAxis, LumaScaling, C);
    B_Hi = scaleTransform(LumaAxis, LumaScaling, B_Hi);
    B_Mi = scaleTransform(LumaAxis, LumaScaling, B_Mi);
    B_Lo = scaleTransform(LumaAxis, LumaScaling, B_Lo);

    C = scaleTransform(SecondaryAxis, SecondaryScaling, C);
    B_Hi = scaleTransform(SecondaryAxis, SecondaryScaling, B_Hi);
    B_Mi = scaleTransform(SecondaryAxis, SecondaryScaling, B_Mi);
    B_Lo = scaleTransform(SecondaryAxis, SecondaryScaling, B_Lo);
	
    float d1 = capsuleDistance(B_Hi, B_Mi, 1., 1., C);
    float d2 = capsuleDistance(B_Mi, B_Lo, 1., 1., C);
	
    float d = min(d1, d2); // or: probabilistic disjunction / union: (d1 + d2 - d1 * d2)
	
	// TODO: Output is 4 channels here, so output more useful information on 3 other channels
    gOutput[p] = d;
}
