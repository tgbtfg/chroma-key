﻿/*
	HLSL Compute Shader
	C : Observed color.
	B : Background Color.
	D : Despilled output. Alpha channel contains despill alpha.
*/
float4 despill(float3 C, float3 B)
{
	// Simple component-average background "Luma",
	// TODO: Support different Luma axis ( Currently 1/3 coefficients, could be sRGB components ) 
    float3 B_avg = dot(B, 1.) / 3.;
    float3 SpillAxis = normalize(B - B_avg); // Color Direction of spill ( Spill chroma / hue )
    float C_spill = dot(C, SpillAxis); // Coordinate of observed color along spill axis 
    float B_spill = dot(B, SpillAxis); // Coordinate of background color along spill axis


	// Compute despill_alpha as linear (in C) gradient along SpillAxis
	// Normalized with respect to by background color
	// The special case C == B implies that despill_alpha == 1.
    float despill_alpha = max(C_spill, 0.) / B_spill;
	
	// Remove background color B proportional to spill amount.
    float3 despilled = C - B * despill_alpha;
	
    float4 D = float4(despilled, despill_alpha);
	
    return D;
}

void main(int groupIndex, int3 dispatchThreadID, int3 groupID, int3 groupThreadID)
{
    int2 p = dispatchThreadID.xy;
	
    float3 C = AJAFeed[p].rgb; // Sample input video, assuming equal input and output dimensions
	
	// Obtain background (despill-) color
	// TODO: Set using constant buffer parameter for efficiency instead.
    float3 B = BackgroundColor[int2(0, 0)].rgb; // Currently single pixel texture...
	
    float4 D = despill(C, B); // Despill Observed color (C) by background color (B)
	
    gOutput[p] = D; // alpha channel now contains despill-alpha
}