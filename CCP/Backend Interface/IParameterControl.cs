﻿using CCP.Backend_Interface;
using System;

namespace CCP
{
    public interface IParameterControl
    {
        object GetParameterValue();
    }

    public interface IParameterControl<T> : IParameterControl where T : IFormattable
    {
        /*
        IParameter parameter
        {
            get;
            set;
        }
        */

        T ParameterValue
        {
            get;
            set;
        }

        string ParameterName
        {
            get;
            set;
        }
    }
}
