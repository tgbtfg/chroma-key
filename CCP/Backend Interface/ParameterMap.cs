﻿using System.Collections.Generic;

namespace CCP
{
    //public class ParameterMap : Dictionary<string, Dictionary<string, string>>
    public class ParameterMap : Dictionary<string, Dictionary<string, object>>
    {
        // TODO: Construct based on pipeline nodes and and parameters
        // TODO: Update entries based on Pipeline changes
        // TODO: Communicate with PipelineControl to synchronize values
        // TODO: Create save/serialize, and load/deserialize fuctionality using JSON

        // public Dictionary<string, Dictionary<string, string>> NodeParameterMap;
        public class NodeEntries : Dictionary<string, object> { };
    }
}
