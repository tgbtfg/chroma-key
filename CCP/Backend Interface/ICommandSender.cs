﻿
//TTMHostApp.TTMProcessHandling
namespace CCP.Backend_Interface
{
    public interface ICommandSender
    {
        /// <summary> Send command. </summary>
        void Send(string command);
    }

    public interface ICommandReceiver
    {
        /// <summary> Send command. </summary>
        void Receive(string command);
    }
}
