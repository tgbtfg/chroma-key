﻿using System;

namespace CCP
{
    public interface IComputePipeline 
    {
        void SetParameter(string nodeName, string parameterName, object parameterValues);
        object[] GetParameterValue(string parameterName);
        // Typically uses GetType(string typeName)
        Type GetParameterType(string parameterName);
        void OpenProject(string projectPath);
        void SetView(string viewName); // TODO: Rename to something more explanatory
        string GetView(); // TODO: Rename to something more explanatory
        void SetInputImage(object/*WrappedImage/ImageData */ image);
        void AddAsset(string assetName, string assetPath);
        void RemoveAsset(string assetName);
        bool IsInitialized();
        bool Connect(string ip, string port);
        /* // Moved to ParameterControlRegistry
        void Connect(IParameterControl control, string nodeName, string parameterName);
        ParameterMap CreateParameterMapFromControls();
        */
    }
}
