﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CCP.Backend_Interface
{
    public interface IParameter
    {
        string name { get; set; }
        string type { get; set; }
    };
    //public enum ParameterType { FLOAT = 0, FLOAT2 = 1, Output = 2 };

    public interface IPipelineNode
    {
        string name { get; set; }
        NodeType type { get; set; }
        List<IParameter> GetParameters();
        bool isImageNode();
    };
    public enum NodeType { Input = 0, ComputeShader = 1, Output = 2 };
    public interface IPipelineGraph
    {
        List<IPipelineNode> GetNodes();
    }
}
